var gulp = require('gulp')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')

// Angularjs app Clinic Services.
gulp.task('app', function () {
    gulp.src(['src/cf/SClinicBundle/Resources/AppAngularjs/**/*.js'])
        .pipe(sourcemaps.init())// Generate and include map.
        .pipe(concat('src/cf/SClinicBundle/Resources/public/js/app/application.js'))
        .pipe(ngAnnotate())
        //.pipe(uglify()) // Minified line.
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'))
});

// External modules, eg: routes, datatables, etc.
gulp.task('external_modules', function () {
    gulp.src(['src/cf/SClinicBundle/Resources/public/vendors/angularjs/**/*.js'])
        .pipe(sourcemaps.init())// Generate and include map.
        .pipe(concat('src/cf/SClinicBundle/Resources/public/vendors/angularjs.modules.min.js'))
        .pipe(ngAnnotate())
        //.pipe(uglify()) // Minified line.
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'))
});

// Copy partials to public folder.
gulp.task('copy_partials', function () {
    gulp.src(['src/cf/SClinicBundle/Resources/AppAngularjs/partials/**/*'])
        .pipe(gulp.dest('src/cf/SClinicBundle/Resources/public/js/app/partials/'))
});

gulp.task('watch', ['app', 'external_modules', 'copy_partials'], function () {
    gulp.watch('src/cf/SClinicBundle/Resources/AppAngularjs/**/*.js', ['app']);
    gulp.watch('src/cf/SClinicBundle/Resources/public/vendors/angularjs/**/*.js', ['external_modules']);
    gulp.watch('src/cf/SClinicBundle/Resources/AppAngularjs/partials/**/*', ['copy_partials']);
}) 