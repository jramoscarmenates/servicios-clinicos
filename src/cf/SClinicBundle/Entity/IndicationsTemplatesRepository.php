<?php
    namespace cf\SClinicBundle\Entity;

    use Doctrine\ORM\EntityRepository;

    class IndicationsTemplatesRepository extends EntityRepository{
        /**
         * Find all indications templates with their each user.
         *
         * @return mixed
         */
        public function findAllWithRelations(){
            $em         = $this->getEntityManager();
            $connection = $em->getConnection();
            $statement  = $connection->prepare("SELECT indications_templates.* , fos_user.username FROM fos_user , indications_templates WHERE indications_templates.create_user_id = fos_user.id");
            $statement->execute();
            $entities = $statement->fetchAll();

            return $entities;
        }
        /**
         * Find indications by id
         *
         */
        public function findById($id){
            $em   = $this->getEntityManager();
            $expr = $em->getExpressionBuilder();
            $qb   = $em->createQueryBuilder();
            $qb->select('entity')->from($this->getEntityName(), 'entity');
            $qb = $qb->andWhere('entity.'.'id'.' ='.$id);
            $qb = $qb->andWhere('entity.status != -1');
            $qb->setMaxResults(1); // Only find One.
            return $qb->getQuery()->getSingleResult();
        }
        /**
         * @param $arguments
         * @param $count
         *
         * @return array
         */
        public function findAllByNotDeleted($arguments, &$count){
            $em   = $this->getEntityManager();
            $expr = $em->getExpressionBuilder();
            extract($arguments);
            //get Count
            $qb_count = $em->createQueryBuilder();
            $qb_count->select('COUNT(entity.id)')->from($this->getEntityName(), 'entity');
            $qb_count = $qb_count->Where('entity.status != -1'); //Status is not deleted
            if(isset($search)){
                $qb_count = $qb_count->andWhere('entity.name'.' LIKE '.':search'.' OR '.'entity.description'.' LIKE '.':search');
                $qb_count->setParameter('search', '%'.$search.'%');
            }
            $count = $qb_count->getQuery()->getSingleScalarResult();
            //Do Query
            $qb    = $em->createQueryBuilder();
            $qb->select('entity')->from($this->getEntityName(), 'entity');
            $qb = $qb->Where('entity.status != -1'); //Status is not deleted
            if(isset($search) && is_string($search)){
                $qb = $qb->andWhere('entity.name'.' LIKE '.':search'.' OR '.'entity.description'.' LIKE '.':search');
                $qb->setParameter('search', '%'.$search.'%');
            }
            if(isset($order_by) && is_string($order_by)){
                $qb = $qb->orderBy($order_by);
            }
            if(isset($limit) && is_numeric($limit)){
                $qb->setMaxResults($limit);
            }
            if(isset($offset) && is_numeric($offset)){
                $qb->setFirstResult($offset);
            }
            $entities = $qb->getQuery()->getResult();

            return $entities;
        }

    }