<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Processing
 *
 * @ORM\Table(name="processing", indexes={@ORM\Index(name="FKprocessing363578", columns={"reportid"})})
 * @ORM\Entity
 */
class Processing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="processing_datetime", type="datetime", nullable=false)
     */
    private $processingDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="images", type="integer", nullable=true)
     */
    private $images;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_datetime", type="integer", nullable=true)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=true)
     */
    private $createUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_datetime", type="integer", nullable=true)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=true)
     */
    private $lastUpdateUserId;

    /**
     * @var \Report
     *
     * @ORM\ManyToOne(targetEntity="Report")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reportid", referencedColumnName="id")
     * })
     */
    private $reportid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return Processing
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set processingDatetime
     *
     * @param \DateTime $processingDatetime
     * @return Processing
     */
    public function setProcessingDatetime($processingDatetime)
    {
        $this->processingDatetime = $processingDatetime;

        return $this;
    }

    /**
     * Get processingDatetime
     *
     * @return \DateTime 
     */
    public function getProcessingDatetime()
    {
        return $this->processingDatetime;
    }

    /**
     * Set images
     *
     * @param integer $images
     * @return Processing
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return integer 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set createDatetime
     *
     * @param integer $createDatetime
     * @return Processing
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return integer 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Processing
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param integer $lastUpdateDatetime
     * @return Processing
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return integer 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Processing
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set reportid
     *
     * @param \cf\SClinicBundle\Entity\Report $reportid
     * @return Processing
     */
    public function setReportid(\cf\SClinicBundle\Entity\Report $reportid = null)
    {
        $this->reportid = $reportid;

        return $this;
    }

    /**
     * Get reportid
     *
     * @return \cf\SClinicBundle\Entity\Report 
     */
    public function getReportid()
    {
        return $this->reportid;
    }
}
