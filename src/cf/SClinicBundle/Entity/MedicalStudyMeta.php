<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MedicalStudyMeta
 *
 * @ORM\Table(name="medical_study_meta", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"}), @ORM\UniqueConstraint(name="parameter", columns={"parameter"})}, indexes={@ORM\Index(name="FKmedical_st3528", columns={"medical_study_id"})})
 * @ORM\Entity
 */
class MedicalStudyMeta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="parameter", type="string", length=100, nullable=false)
     */
    private $parameter;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=100, nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=true)
     */
    private $createUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_datetime", type="integer", nullable=true)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=true)
     */
    private $lastUpdateUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_datetime", type="integer", nullable=true)
     */
    private $lastUpdateDatetime;

    /**
     * @var \MedicalStudy
     *
     * @ORM\ManyToOne(targetEntity="MedicalStudy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medical_study_id", referencedColumnName="id")
     * })
     */
    private $medicalStudy;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MedicalStudyMeta
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parameter
     *
     * @param string $parameter
     * @return MedicalStudyMeta
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter
     *
     * @return string 
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return MedicalStudyMeta
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MedicalStudyMeta
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MedicalStudyMeta
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return MedicalStudyMeta
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set createDatetime
     *
     * @param integer $createDatetime
     * @return MedicalStudyMeta
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return integer 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return MedicalStudyMeta
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param integer $lastUpdateDatetime
     * @return MedicalStudyMeta
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return integer 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set medicalStudy
     *
     * @param \cf\SClinicBundle\Entity\MedicalStudy $medicalStudy
     * @return MedicalStudyMeta
     */
    public function setMedicalStudy(\cf\SClinicBundle\Entity\MedicalStudy $medicalStudy = null)
    {
        $this->medicalStudy = $medicalStudy;

        return $this;
    }

    /**
     * Get medicalStudy
     *
     * @return \cf\SClinicBundle\Entity\MedicalStudy 
     */
    public function getMedicalStudy()
    {
        return $this->medicalStudy;
    }
}
