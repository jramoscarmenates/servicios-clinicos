<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportTemplates
 *
 * @ORM\Table(name="report_templates", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity
 */
class ReportTemplates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="template_pre_document", type="text", nullable=true)
     */
    private $templatePreDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="template_document", type="text", nullable=true)
     */
    private $templateDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="template_post_document", type="text", nullable=true)
     */
    private $templatePostDocument;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ReportTemplates
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set templatePreDocument
     *
     * @param string $templatePreDocument
     * @return ReportTemplates
     */
    public function setTemplatePreDocument($templatePreDocument)
    {
        $this->templatePreDocument = $templatePreDocument;

        return $this;
    }

    /**
     * Get templatePreDocument
     *
     * @return string 
     */
    public function getTemplatePreDocument()
    {
        return $this->templatePreDocument;
    }

    /**
     * Set templateDocument
     *
     * @param string $templateDocument
     * @return ReportTemplates
     */
    public function setTemplateDocument($templateDocument)
    {
        $this->templateDocument = $templateDocument;

        return $this;
    }

    /**
     * Get templateDocument
     *
     * @return string 
     */
    public function getTemplateDocument()
    {
        return $this->templateDocument;
    }

    /**
     * Set templatePostDocument
     *
     * @param string $templatePostDocument
     * @return ReportTemplates
     */
    public function setTemplatePostDocument($templatePostDocument)
    {
        $this->templatePostDocument = $templatePostDocument;

        return $this;
    }

    /**
     * Get templatePostDocument
     *
     * @return string 
     */
    public function getTemplatePostDocument()
    {
        return $this->templatePostDocument;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return ReportTemplates
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ReportTemplates
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return ReportTemplates
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return ReportTemplates
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return ReportTemplates
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return ReportTemplates
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }
}
