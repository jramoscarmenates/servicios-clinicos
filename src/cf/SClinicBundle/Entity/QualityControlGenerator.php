<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QualityControlGenerator
 *
 * @ORM\Table(name="quality_control_generator", indexes={@ORM\Index(name="FKquality_co812744", columns={"generatorid"})})
 * @ORM\Entity
 */
class QualityControlGenerator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var \Generator
     *
     * @ORM\ManyToOne(targetEntity="Generator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="generatorid", referencedColumnName="id")
     * })
     */
    private $generatorid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return QualityControlGenerator
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return QualityControlGenerator
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set generatorid
     *
     * @param \cf\SClinicBundle\Entity\Generator $generatorid
     * @return QualityControlGenerator
     */
    public function setGeneratorid(\cf\SClinicBundle\Entity\Generator $generatorid = null)
    {
        $this->generatorid = $generatorid;

        return $this;
    }

    /**
     * Get generatorid
     *
     * @return \cf\SClinicBundle\Entity\Generator 
     */
    public function getGeneratorid()
    {
        return $this->generatorid;
    }
}
