<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activimeter
 *
 * @ORM\Table(name="activimeter", uniqueConstraints={@ORM\UniqueConstraint(name="stock_number", columns={"stock_number"})}, indexes={@ORM\Index(name="FKactivimete982471", columns={"local_id"})})
 * @ORM\Entity
 */
class Activimeter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="stock_number", type="string", length=100, nullable=false)
     */
    private $stockNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="mark", type="string", length=100, nullable=true)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=100, nullable=true)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="production_datetime", type="datetime", nullable=true)
     */
    private $productionDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_calibration_datetime", type="datetime", nullable=true)
     */
    private $lastCalibrationDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="last_calibration_certified", type="string", length=100, nullable=true)
     */
    private $lastCalibrationCertified;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Locals
     *
     * @ORM\ManyToOne(targetEntity="Locals")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="local_id", referencedColumnName="id")
     * })
     */
    private $local;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stockNumber
     *
     * @param string $stockNumber
     * @return Activimeter
     */
    public function setStockNumber($stockNumber)
    {
        $this->stockNumber = $stockNumber;

        return $this;
    }

    /**
     * Get stockNumber
     *
     * @return string 
     */
    public function getStockNumber()
    {
        return $this->stockNumber;
    }

    /**
     * Set mark
     *
     * @param string $mark
     * @return Activimeter
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Activimeter
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return Activimeter
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set productionDatetime
     *
     * @param \DateTime $productionDatetime
     * @return Activimeter
     */
    public function setProductionDatetime($productionDatetime)
    {
        $this->productionDatetime = $productionDatetime;

        return $this;
    }

    /**
     * Get productionDatetime
     *
     * @return \DateTime 
     */
    public function getProductionDatetime()
    {
        return $this->productionDatetime;
    }

    /**
     * Set lastCalibrationDatetime
     *
     * @param \DateTime $lastCalibrationDatetime
     * @return Activimeter
     */
    public function setLastCalibrationDatetime($lastCalibrationDatetime)
    {
        $this->lastCalibrationDatetime = $lastCalibrationDatetime;

        return $this;
    }

    /**
     * Get lastCalibrationDatetime
     *
     * @return \DateTime 
     */
    public function getLastCalibrationDatetime()
    {
        return $this->lastCalibrationDatetime;
    }

    /**
     * Set lastCalibrationCertified
     *
     * @param string $lastCalibrationCertified
     * @return Activimeter
     */
    public function setLastCalibrationCertified($lastCalibrationCertified)
    {
        $this->lastCalibrationCertified = $lastCalibrationCertified;

        return $this;
    }

    /**
     * Get lastCalibrationCertified
     *
     * @return string 
     */
    public function getLastCalibrationCertified()
    {
        return $this->lastCalibrationCertified;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Activimeter
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Activimeter
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Activimeter
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Activimeter
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Activimeter
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Activimeter
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Activimeter
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set local
     *
     * @param \cf\SClinicBundle\Entity\Locals $local
     * @return Activimeter
     */
    public function setLocal(\cf\SClinicBundle\Entity\Locals $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \cf\SClinicBundle\Entity\Locals 
     */
    public function getLocal()
    {
        return $this->local;
    }
}
