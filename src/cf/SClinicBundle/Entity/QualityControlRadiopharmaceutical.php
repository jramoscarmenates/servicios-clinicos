<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QualityControlRadiopharmaceutical
 *
 * @ORM\Table(name="quality_control_radiopharmaceutical", indexes={@ORM\Index(name="FKquality_co221702", columns={"radiopharmaceutical_id"})})
 * @ORM\Entity
 */
class QualityControlRadiopharmaceutical
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="description", type="integer", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="observation", type="integer", nullable=true)
     */
    private $observation;

    /**
     * @var \Radiopharmaceutical
     *
     * @ORM\ManyToOne(targetEntity="Radiopharmaceutical")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="radiopharmaceutical_id", referencedColumnName="id")
     * })
     */
    private $radiopharmaceutical;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param integer $description
     * @return QualityControlRadiopharmaceutical
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return integer 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set observation
     *
     * @param integer $observation
     * @return QualityControlRadiopharmaceutical
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return integer 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set radiopharmaceutical
     *
     * @param \cf\SClinicBundle\Entity\Radiopharmaceutical $radiopharmaceutical
     * @return QualityControlRadiopharmaceutical
     */
    public function setRadiopharmaceutical(\cf\SClinicBundle\Entity\Radiopharmaceutical $radiopharmaceutical = null)
    {
        $this->radiopharmaceutical = $radiopharmaceutical;

        return $this;
    }

    /**
     * Get radiopharmaceutical
     *
     * @return \cf\SClinicBundle\Entity\Radiopharmaceutical 
     */
    public function getRadiopharmaceutical()
    {
        return $this->radiopharmaceutical;
    }
}
