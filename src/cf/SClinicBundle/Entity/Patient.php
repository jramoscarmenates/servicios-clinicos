<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Patient
 *
 * @ORM\Table(name="patient", uniqueConstraints={@ORM\UniqueConstraint(name="medical_record_id", columns={"medical_record_id"}), @ORM\UniqueConstraint(name="personal_id", columns={"personal_id"})}, indexes={@ORM\Index(name="FKpatient863833", columns={"region_id"}), @ORM\Index(name="FKpatient499430", columns={"town_id"}), @ORM\Index(name="FKpatient385103", columns={"nationalityid"})})
 * @ORM\Entity
 */
class Patient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_id", type="string", length=50, nullable=true)
     */
    private $personalId;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="up_date", type="date", nullable=true)
     */
    private $upDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_foreign", type="smallint", nullable=false)
     */
    private $isForeign;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_birth", type="date", nullable=true)
     */
    private $dateBirth;

    /**
     * @var integer
     *
     * @ORM\Column(name="medical_record_id", type="integer", nullable=false)
     */
    private $medicalRecordId;

    /**
     * @var string
     *
     * @ORM\Column(name="medical_record", type="text", nullable=true)
     */
    private $medicalRecord;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="notification_email", type="smallint", nullable=false)
     */
    private $notificationEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=100, nullable=true)
     */
    private $telephone;

    /**
     * @var integer
     *
     * @ORM\Column(name="sex", type="smallint", nullable=false)
     */
    private $sex;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Nationality
     *
     * @ORM\ManyToOne(targetEntity="Nationality")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nationalityid", referencedColumnName="id")
     * })
     */
    private $nationalityid;

    /**
     * @var \Town
     *
     * @ORM\ManyToOne(targetEntity="Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id")
     * })
     */
    private $town;

    /**
     * @var \Region
     *
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personalId
     *
     * @param string $personalId
     * @return Patient
     */
    public function setPersonalId($personalId)
    {
        $this->personalId = $personalId;

        return $this;
    }

    /**
     * Get personalId
     *
     * @return string 
     */
    public function getPersonalId()
    {
        return $this->personalId;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Patient
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Patient
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Patient
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set upDate
     *
     * @param \DateTime $upDate
     * @return Patient
     */
    public function setUpDate($upDate)
    {
        $this->upDate = $upDate;

        return $this;
    }

    /**
     * Get upDate
     *
     * @return \DateTime 
     */
    public function getUpDate()
    {
        return $this->upDate;
    }

    /**
     * Set isForeign
     *
     * @param integer $isForeign
     * @return Patient
     */
    public function setIsForeign($isForeign)
    {
        $this->isForeign = $isForeign;

        return $this;
    }

    /**
     * Get isForeign
     *
     * @return integer 
     */
    public function getIsForeign()
    {
        return $this->isForeign;
    }

    /**
     * Set dateBirth
     *
     * @param \DateTime $dateBirth
     * @return Patient
     */
    public function setDateBirth($dateBirth)
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }

    /**
     * Get dateBirth
     *
     * @return \DateTime 
     */
    public function getDateBirth()
    {
        return $this->dateBirth;
    }

    /**
     * Set medicalRecordId
     *
     * @param integer $medicalRecordId
     * @return Patient
     */
    public function setMedicalRecordId($medicalRecordId)
    {
        $this->medicalRecordId = $medicalRecordId;

        return $this;
    }

    /**
     * Get medicalRecordId
     *
     * @return integer 
     */
    public function getMedicalRecordId()
    {
        return $this->medicalRecordId;
    }

    /**
     * Set medicalRecord
     *
     * @param string $medicalRecord
     * @return Patient
     */
    public function setMedicalRecord($medicalRecord)
    {
        $this->medicalRecord = $medicalRecord;

        return $this;
    }

    /**
     * Get medicalRecord
     *
     * @return string 
     */
    public function getMedicalRecord()
    {
        return $this->medicalRecord;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Patient
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set notificationEmail
     *
     * @param integer $notificationEmail
     * @return Patient
     */
    public function setNotificationEmail($notificationEmail)
    {
        $this->notificationEmail = $notificationEmail;

        return $this;
    }

    /**
     * Get notificationEmail
     *
     * @return integer 
     */
    public function getNotificationEmail()
    {
        return $this->notificationEmail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Patient
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return Patient
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Patient
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Patient
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Patient
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Patient
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Patient
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Patient
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Patient
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set nationalityid
     *
     * @param \cf\SClinicBundle\Entity\Nationality $nationalityid
     * @return Patient
     */
    public function setNationalityid(\cf\SClinicBundle\Entity\Nationality $nationalityid = null)
    {
        $this->nationalityid = $nationalityid;

        return $this;
    }

    /**
     * Get nationalityid
     *
     * @return \cf\SClinicBundle\Entity\Nationality 
     */
    public function getNationalityid()
    {
        return $this->nationalityid;
    }

    /**
     * Set town
     *
     * @param \cf\SClinicBundle\Entity\Town $town
     * @return Patient
     */
    public function setTown(\cf\SClinicBundle\Entity\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \cf\SClinicBundle\Entity\Town 
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set region
     *
     * @param \cf\SClinicBundle\Entity\Region $region
     * @return Patient
     */
    public function setRegion(\cf\SClinicBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \cf\SClinicBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }
}
