<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="smallint", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="region_name", type="string", length=100, nullable=true)
     */
    private $regionName;

    /**
     * @var integer
     *
     * @ORM\Column(name="dpa_code", type="integer", nullable=false)
     */
    private $dpaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Region
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set regionName
     *
     * @param string $regionName
     * @return Region
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;

        return $this;
    }

    /**
     * Get regionName
     *
     * @return string 
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * Set dpaCode
     *
     * @param integer $dpaCode
     * @return Region
     */
    public function setDpaCode($dpaCode)
    {
        $this->dpaCode = $dpaCode;

        return $this;
    }

    /**
     * Get dpaCode
     *
     * @return integer 
     */
    public function getDpaCode()
    {
        return $this->dpaCode;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Region
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Region
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Region
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Region
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Region
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }
}
