<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Radiopharmaceutical
 *
 * @ORM\Table(name="radiopharmaceutical", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})}, indexes={@ORM\Index(name="FKradiopharm286009", columns={"types_radiopharmaceuticalid"})})
 * @ORM\Entity
 */
class Radiopharmaceutical
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="batch", type="string", length=100, nullable=true)
     */
    private $batch;

    /**
     * @var string
     *
     * @ORM\Column(name="mark", type="string", length=100, nullable=true)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=100, nullable=true)
     */
    private $model;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="marcage_datetime", type="datetime", nullable=true)
     */
    private $marcageDatetime;

    /**
     * @var float
     *
     * @ORM\Column(name="marcage_activity", type="float", precision=10, scale=0, nullable=true)
     */
    private $marcageActivity;

    /**
     * @var float
     *
     * @ORM\Column(name="marcage_volumen", type="float", precision=10, scale=0, nullable=true)
     */
    private $marcageVolumen;

    /**
     * @var float
     *
     * @ORM\Column(name="last_volumen", type="float", precision=10, scale=0, nullable=true)
     */
    private $lastVolumen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration datetime", type="datetime", nullable=true)
     */
    private $expirationDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="production datetime", type="datetime", nullable=true)
     */
    private $productionDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="description", type="integer", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="observation", type="integer", nullable=true)
     */
    private $observation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \TypesRadiopharmaceutical
     *
     * @ORM\ManyToOne(targetEntity="TypesRadiopharmaceutical")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="types_radiopharmaceuticalid", referencedColumnName="id")
     * })
     */
    private $typesRadiopharmaceuticalid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Radiopharmaceutical
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set batch
     *
     * @param string $batch
     * @return Radiopharmaceutical
     */
    public function setBatch($batch)
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * Get batch
     *
     * @return string 
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * Set mark
     *
     * @param string $mark
     * @return Radiopharmaceutical
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Radiopharmaceutical
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set marcageDatetime
     *
     * @param \DateTime $marcageDatetime
     * @return Radiopharmaceutical
     */
    public function setMarcageDatetime($marcageDatetime)
    {
        $this->marcageDatetime = $marcageDatetime;

        return $this;
    }

    /**
     * Get marcageDatetime
     *
     * @return \DateTime 
     */
    public function getMarcageDatetime()
    {
        return $this->marcageDatetime;
    }

    /**
     * Set marcageActivity
     *
     * @param float $marcageActivity
     * @return Radiopharmaceutical
     */
    public function setMarcageActivity($marcageActivity)
    {
        $this->marcageActivity = $marcageActivity;

        return $this;
    }

    /**
     * Get marcageActivity
     *
     * @return float 
     */
    public function getMarcageActivity()
    {
        return $this->marcageActivity;
    }

    /**
     * Set marcageVolumen
     *
     * @param float $marcageVolumen
     * @return Radiopharmaceutical
     */
    public function setMarcageVolumen($marcageVolumen)
    {
        $this->marcageVolumen = $marcageVolumen;

        return $this;
    }

    /**
     * Get marcageVolumen
     *
     * @return float 
     */
    public function getMarcageVolumen()
    {
        return $this->marcageVolumen;
    }

    /**
     * Set lastVolumen
     *
     * @param float $lastVolumen
     * @return Radiopharmaceutical
     */
    public function setLastVolumen($lastVolumen)
    {
        $this->lastVolumen = $lastVolumen;

        return $this;
    }

    /**
     * Get lastVolumen
     *
     * @return float 
     */
    public function getLastVolumen()
    {
        return $this->lastVolumen;
    }

    /**
     * Set expirationDatetime
     *
     * @param \DateTime $expirationDatetime
     * @return Radiopharmaceutical
     */
    public function setExpirationDatetime($expirationDatetime)
    {
        $this->expirationDatetime = $expirationDatetime;

        return $this;
    }

    /**
     * Get expirationDatetime
     *
     * @return \DateTime 
     */
    public function getExpirationDatetime()
    {
        return $this->expirationDatetime;
    }

    /**
     * Set productionDatetime
     *
     * @param \DateTime $productionDatetime
     * @return Radiopharmaceutical
     */
    public function setProductionDatetime($productionDatetime)
    {
        $this->productionDatetime = $productionDatetime;

        return $this;
    }

    /**
     * Get productionDatetime
     *
     * @return \DateTime 
     */
    public function getProductionDatetime()
    {
        return $this->productionDatetime;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return Radiopharmaceutical
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Radiopharmaceutical
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param integer $description
     * @return Radiopharmaceutical
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return integer 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set observation
     *
     * @param integer $observation
     * @return Radiopharmaceutical
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return integer 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Radiopharmaceutical
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Radiopharmaceutical
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Radiopharmaceutical
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Radiopharmaceutical
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set typesRadiopharmaceuticalid
     *
     * @param \cf\SClinicBundle\Entity\TypesRadiopharmaceutical $typesRadiopharmaceuticalid
     * @return Radiopharmaceutical
     */
    public function setTypesRadiopharmaceuticalid(\cf\SClinicBundle\Entity\TypesRadiopharmaceutical $typesRadiopharmaceuticalid = null)
    {
        $this->typesRadiopharmaceuticalid = $typesRadiopharmaceuticalid;

        return $this;
    }

    /**
     * Get typesRadiopharmaceuticalid
     *
     * @return \cf\SClinicBundle\Entity\TypesRadiopharmaceutical 
     */
    public function getTypesRadiopharmaceuticalid()
    {
        return $this->typesRadiopharmaceuticalid;
    }
}
