<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity
 *
 * @ORM\Table(name="entity", indexes={@ORM\Index(name="FKentity614403", columns={"town_id"}), @ORM\Index(name="FKentity250000", columns={"region_id"})})
 * @ORM\Entity
 */
class Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="smallint", nullable=false)
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(name="reeup_code", type="integer", nullable=true)
     */
    private $reeupCode;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="acronyms", type="string", length=50, nullable=true)
     */
    private $acronyms;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="oace_osde_id", type="integer", nullable=true)
     */
    private $oaceOsdeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="nae_code", type="integer", nullable=true)
     */
    private $naeCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="forg", type="integer", nullable=true)
     */
    private $forg;

    /**
     * @var integer
     *
     * @ORM\Column(name="ffi", type="integer", nullable=true)
     */
    private $ffi;

    /**
     * @var integer
     *
     * @ORM\Column(name="uni", type="integer", nullable=true)
     */
    private $uni;

    /**
     * @var integer
     *
     * @ORM\Column(name="sub", type="integer", nullable=true)
     */
    private $sub;

    /**
     * @var integer
     *
     * @ORM\Column(name="subord", type="integer", nullable=true)
     */
    private $subord;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="ep", type="integer", nullable=true)
     */
    private $ep;

    /**
     * @var integer
     *
     * @ORM\Column(name="phones", type="integer", nullable=true)
     */
    private $phones;

    /**
     * @var integer
     *
     * @ORM\Column(name="fax", type="integer", nullable=true)
     */
    private $fax;

    /**
     * @var integer
     *
     * @ORM\Column(name="email", type="integer", nullable=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Region
     *
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var \Town
     *
     * @ORM\ManyToOne(targetEntity="Town")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="town_id", referencedColumnName="id")
     * })
     */
    private $town;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Entity
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set reeupCode
     *
     * @param integer $reeupCode
     * @return Entity
     */
    public function setReeupCode($reeupCode)
    {
        $this->reeupCode = $reeupCode;

        return $this;
    }

    /**
     * Get reeupCode
     *
     * @return integer 
     */
    public function getReeupCode()
    {
        return $this->reeupCode;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Entity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set acronyms
     *
     * @param string $acronyms
     * @return Entity
     */
    public function setAcronyms($acronyms)
    {
        $this->acronyms = $acronyms;

        return $this;
    }

    /**
     * Get acronyms
     *
     * @return string 
     */
    public function getAcronyms()
    {
        return $this->acronyms;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Entity
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set oaceOsdeId
     *
     * @param integer $oaceOsdeId
     * @return Entity
     */
    public function setOaceOsdeId($oaceOsdeId)
    {
        $this->oaceOsdeId = $oaceOsdeId;

        return $this;
    }

    /**
     * Get oaceOsdeId
     *
     * @return integer 
     */
    public function getOaceOsdeId()
    {
        return $this->oaceOsdeId;
    }

    /**
     * Set naeCode
     *
     * @param integer $naeCode
     * @return Entity
     */
    public function setNaeCode($naeCode)
    {
        $this->naeCode = $naeCode;

        return $this;
    }

    /**
     * Get naeCode
     *
     * @return integer 
     */
    public function getNaeCode()
    {
        return $this->naeCode;
    }

    /**
     * Set forg
     *
     * @param integer $forg
     * @return Entity
     */
    public function setForg($forg)
    {
        $this->forg = $forg;

        return $this;
    }

    /**
     * Get forg
     *
     * @return integer 
     */
    public function getForg()
    {
        return $this->forg;
    }

    /**
     * Set ffi
     *
     * @param integer $ffi
     * @return Entity
     */
    public function setFfi($ffi)
    {
        $this->ffi = $ffi;

        return $this;
    }

    /**
     * Get ffi
     *
     * @return integer 
     */
    public function getFfi()
    {
        return $this->ffi;
    }

    /**
     * Set uni
     *
     * @param integer $uni
     * @return Entity
     */
    public function setUni($uni)
    {
        $this->uni = $uni;

        return $this;
    }

    /**
     * Get uni
     *
     * @return integer 
     */
    public function getUni()
    {
        return $this->uni;
    }

    /**
     * Set sub
     *
     * @param integer $sub
     * @return Entity
     */
    public function setSub($sub)
    {
        $this->sub = $sub;

        return $this;
    }

    /**
     * Get sub
     *
     * @return integer 
     */
    public function getSub()
    {
        return $this->sub;
    }

    /**
     * Set subord
     *
     * @param integer $subord
     * @return Entity
     */
    public function setSubord($subord)
    {
        $this->subord = $subord;

        return $this;
    }

    /**
     * Get subord
     *
     * @return integer 
     */
    public function getSubord()
    {
        return $this->subord;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Entity
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set ep
     *
     * @param integer $ep
     * @return Entity
     */
    public function setEp($ep)
    {
        $this->ep = $ep;

        return $this;
    }

    /**
     * Get ep
     *
     * @return integer 
     */
    public function getEp()
    {
        return $this->ep;
    }

    /**
     * Set phones
     *
     * @param integer $phones
     * @return Entity
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return integer 
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set fax
     *
     * @param integer $fax
     * @return Entity
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return integer 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param integer $email
     * @return Entity
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return integer 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Entity
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Entity
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Entity
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Entity
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set region
     *
     * @param \cf\SClinicBundle\Entity\Region $region
     * @return Entity
     */
    public function setRegion(\cf\SClinicBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \cf\SClinicBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set town
     *
     * @param \cf\SClinicBundle\Entity\Town $town
     * @return Entity
     */
    public function setTown(\cf\SClinicBundle\Entity\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \cf\SClinicBundle\Entity\Town 
     */
    public function getTown()
    {
        return $this->town;
    }
}
