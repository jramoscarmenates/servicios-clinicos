<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypesRadiopharmaceutical
 *
 * @ORM\Table(name="types_radiopharmaceutical", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity
 */
class TypesRadiopharmaceutical
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="MedicalStudy", inversedBy="typesRadiopharmaceutical")
     * @ORM\JoinTable(name="types_radiopharmaceutical_medical_study",
     *   joinColumns={
     *     @ORM\JoinColumn(name="types_radiopharmaceutical_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="medical_study_id", referencedColumnName="id")
     *   }
     * )
     */
    private $medicalStudy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medicalStudy = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TypesRadiopharmaceutical
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TypesRadiopharmaceutical
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TypesRadiopharmaceutical
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return TypesRadiopharmaceutical
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return TypesRadiopharmaceutical
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return TypesRadiopharmaceutical
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return TypesRadiopharmaceutical
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Add medicalStudy
     *
     * @param \cf\SClinicBundle\Entity\MedicalStudy $medicalStudy
     * @return TypesRadiopharmaceutical
     */
    public function addMedicalStudy(\cf\SClinicBundle\Entity\MedicalStudy $medicalStudy)
    {
        $this->medicalStudy[] = $medicalStudy;

        return $this;
    }

    /**
     * Remove medicalStudy
     *
     * @param \cf\SClinicBundle\Entity\MedicalStudy $medicalStudy
     */
    public function removeMedicalStudy(\cf\SClinicBundle\Entity\MedicalStudy $medicalStudy)
    {
        $this->medicalStudy->removeElement($medicalStudy);
    }

    /**
     * Get medicalStudy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedicalStudy()
    {
        return $this->medicalStudy;
    }
}
