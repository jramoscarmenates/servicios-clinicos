<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IndicationsTemplates
 *
 * @ORM\Table(name="indications_templates", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity(repositoryClass="cf\SClinicBundle\Entity\IndicationsTemplatesRepository")
 */
class IndicationsTemplates {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=100, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="template_indication", type="text", nullable=true)
	 */
	private $templateIndication;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="smallint", nullable=false)
	 */
	private $status;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
	 */
	private $createDatetime;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="create_user_id", type="integer", nullable=false)
	 */
	private $createUserId;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
	 */
	private $lastUpdateDatetime;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
	 */
	private $lastUpdateUserId;

	/**
	 * @param $fosUser
	 */
	public function __construct( $fosUser ) {

		//Case that not user.
		$this->setCreateUserId( -1 );
		$this->setLastUpdateUserId( -1 );

		if(isset($fosUser) && !empty($fosUser)) {
			if ($fosUser->getId()) {
				$this->setCreateUserId($fosUser->getId());
				$this->setLastUpdateUserId($fosUser->getId());
			}
		}

		$this->setCreateDatetime( new \DateTime() );
		$this->setLastUpdateDatetime( new \DateTime() );

	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return IndicationsTemplates
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set templateIndication
	 *
	 * @param string $templateIndication
	 *
	 * @return IndicationsTemplates
	 */
	public function setTemplateIndication( $templateIndication ) {
		$this->templateIndication = $templateIndication;

		return $this;
	}

	/**
	 * Get templateIndication
	 *
	 * @return string
	 */
	public function getTemplateIndication() {
		return $this->templateIndication;
	}

	/**
	 * Set status
	 *
	 * @param integer $status
	 *
	 * @return IndicationsTemplates
	 */
	public function setStatus( $status ) {
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status
	 *
	 * @return integer
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return IndicationsTemplates
	 */
	public function setDescription( $description ) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set createDatetime
	 *
	 * @param \DateTime $createDatetime
	 *
	 * @return IndicationsTemplates
	 */
	public function setCreateDatetime( $createDatetime ) {
		$this->createDatetime = $createDatetime;

		return $this;
	}

	/**
	 * Get createDatetime
	 *
	 * @return \DateTime
	 */
	public function getCreateDatetime() {
		return $this->createDatetime;
	}

	/**
	 * Set createUserId
	 *
	 * @param integer $createUserId
	 *
	 * @return IndicationsTemplates
	 */
	public function setCreateUserId( $createUserId ) {
		$this->createUserId = $createUserId;

		return $this;
	}

	/**
	 * Get createUserId
	 *
	 * @return integer
	 */
	public function getCreateUserId() {
		return $this->createUserId;
	}

	/**
	 * Set lastUpdateDatetime
	 *
	 * @param \DateTime $lastUpdateDatetime
	 *
	 * @return IndicationsTemplates
	 */
	public function setLastUpdateDatetime( $lastUpdateDatetime ) {
		$this->lastUpdateDatetime = $lastUpdateDatetime;

		return $this;
	}

	/**
	 * Get lastUpdateDatetime
	 *
	 * @return \DateTime
	 */
	public function getLastUpdateDatetime() {
		return $this->lastUpdateDatetime;
	}

	/**
	 * Set lastUpdateUserId
	 *
	 * @param integer $lastUpdateUserId
	 *
	 * @return IndicationsTemplates
	 */
	public function setLastUpdateUserId( $lastUpdateUserId ) {
		$this->lastUpdateUserId = $lastUpdateUserId;

		return $this;
	}

	/**
	 * Get lastUpdateUserId
	 *
	 * @return integer
	 */
	public function getLastUpdateUserId() {
		return $this->lastUpdateUserId;
	}
}
