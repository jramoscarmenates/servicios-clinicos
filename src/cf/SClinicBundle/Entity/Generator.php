<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Generator
 *
 * @ORM\Table(name="generator", indexes={@ORM\Index(name="FKgenerator294023", columns={"local_id"})})
 * @ORM\Entity
 */
class Generator
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="batch", type="string", length=50, nullable=false)
     */
    private $batch;

    /**
     * @var string
     *
     * @ORM\Column(name="mark", type="string", length=100, nullable=true)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=100, nullable=true)
     */
    private $model;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reception_datetime", type="datetime", nullable=false)
     */
    private $receptionDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="waste_datetime", type="datetime", nullable=true)
     */
    private $wasteDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="factory_datetime_reference", type="datetime", nullable=true)
     */
    private $factoryDatetimeReference;

    /**
     * @var float
     *
     * @ORM\Column(name="factory_activity_reference", type="float", precision=10, scale=0, nullable=true)
     */
    private $factoryActivityReference;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="factory_production_datetime", type="datetime", nullable=true)
     */
    private $factoryProductionDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="factory_expiracion_datetime", type="datetime", nullable=true)
     */
    private $factoryExpiracionDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="first_datetime_elucion", type="datetime", nullable=true)
     */
    private $firstDatetimeElucion;

    /**
     * @var float
     *
     * @ORM\Column(name="first_activity_elucion", type="float", precision=10, scale=0, nullable=true)
     */
    private $firstActivityElucion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_datetime_elucion", type="datetime", nullable=true)
     */
    private $lastDatetimeElucion;

    /**
     * @var float
     *
     * @ORM\Column(name="last_activity_elucion", type="float", precision=10, scale=0, nullable=true)
     */
    private $lastActivityElucion;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Locals
     *
     * @ORM\ManyToOne(targetEntity="Locals")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="local_id", referencedColumnName="id")
     * })
     */
    private $local;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Generator
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Generator
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set batch
     *
     * @param string $batch
     * @return Generator
     */
    public function setBatch($batch)
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * Get batch
     *
     * @return string 
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * Set mark
     *
     * @param string $mark
     * @return Generator
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Generator
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set receptionDatetime
     *
     * @param \DateTime $receptionDatetime
     * @return Generator
     */
    public function setReceptionDatetime($receptionDatetime)
    {
        $this->receptionDatetime = $receptionDatetime;

        return $this;
    }

    /**
     * Get receptionDatetime
     *
     * @return \DateTime 
     */
    public function getReceptionDatetime()
    {
        return $this->receptionDatetime;
    }

    /**
     * Set wasteDatetime
     *
     * @param \DateTime $wasteDatetime
     * @return Generator
     */
    public function setWasteDatetime($wasteDatetime)
    {
        $this->wasteDatetime = $wasteDatetime;

        return $this;
    }

    /**
     * Get wasteDatetime
     *
     * @return \DateTime 
     */
    public function getWasteDatetime()
    {
        return $this->wasteDatetime;
    }

    /**
     * Set factoryDatetimeReference
     *
     * @param \DateTime $factoryDatetimeReference
     * @return Generator
     */
    public function setFactoryDatetimeReference($factoryDatetimeReference)
    {
        $this->factoryDatetimeReference = $factoryDatetimeReference;

        return $this;
    }

    /**
     * Get factoryDatetimeReference
     *
     * @return \DateTime 
     */
    public function getFactoryDatetimeReference()
    {
        return $this->factoryDatetimeReference;
    }

    /**
     * Set factoryActivityReference
     *
     * @param float $factoryActivityReference
     * @return Generator
     */
    public function setFactoryActivityReference($factoryActivityReference)
    {
        $this->factoryActivityReference = $factoryActivityReference;

        return $this;
    }

    /**
     * Get factoryActivityReference
     *
     * @return float 
     */
    public function getFactoryActivityReference()
    {
        return $this->factoryActivityReference;
    }

    /**
     * Set factoryProductionDatetime
     *
     * @param \DateTime $factoryProductionDatetime
     * @return Generator
     */
    public function setFactoryProductionDatetime($factoryProductionDatetime)
    {
        $this->factoryProductionDatetime = $factoryProductionDatetime;

        return $this;
    }

    /**
     * Get factoryProductionDatetime
     *
     * @return \DateTime 
     */
    public function getFactoryProductionDatetime()
    {
        return $this->factoryProductionDatetime;
    }

    /**
     * Set factoryExpiracionDatetime
     *
     * @param \DateTime $factoryExpiracionDatetime
     * @return Generator
     */
    public function setFactoryExpiracionDatetime($factoryExpiracionDatetime)
    {
        $this->factoryExpiracionDatetime = $factoryExpiracionDatetime;

        return $this;
    }

    /**
     * Get factoryExpiracionDatetime
     *
     * @return \DateTime 
     */
    public function getFactoryExpiracionDatetime()
    {
        return $this->factoryExpiracionDatetime;
    }

    /**
     * Set firstDatetimeElucion
     *
     * @param \DateTime $firstDatetimeElucion
     * @return Generator
     */
    public function setFirstDatetimeElucion($firstDatetimeElucion)
    {
        $this->firstDatetimeElucion = $firstDatetimeElucion;

        return $this;
    }

    /**
     * Get firstDatetimeElucion
     *
     * @return \DateTime 
     */
    public function getFirstDatetimeElucion()
    {
        return $this->firstDatetimeElucion;
    }

    /**
     * Set firstActivityElucion
     *
     * @param float $firstActivityElucion
     * @return Generator
     */
    public function setFirstActivityElucion($firstActivityElucion)
    {
        $this->firstActivityElucion = $firstActivityElucion;

        return $this;
    }

    /**
     * Get firstActivityElucion
     *
     * @return float 
     */
    public function getFirstActivityElucion()
    {
        return $this->firstActivityElucion;
    }

    /**
     * Set lastDatetimeElucion
     *
     * @param \DateTime $lastDatetimeElucion
     * @return Generator
     */
    public function setLastDatetimeElucion($lastDatetimeElucion)
    {
        $this->lastDatetimeElucion = $lastDatetimeElucion;

        return $this;
    }

    /**
     * Get lastDatetimeElucion
     *
     * @return \DateTime 
     */
    public function getLastDatetimeElucion()
    {
        return $this->lastDatetimeElucion;
    }

    /**
     * Set lastActivityElucion
     *
     * @param float $lastActivityElucion
     * @return Generator
     */
    public function setLastActivityElucion($lastActivityElucion)
    {
        $this->lastActivityElucion = $lastActivityElucion;

        return $this;
    }

    /**
     * Get lastActivityElucion
     *
     * @return float 
     */
    public function getLastActivityElucion()
    {
        return $this->lastActivityElucion;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return Generator
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Generator
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Generator
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Generator
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Generator
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Generator
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Generator
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Generator
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set local
     *
     * @param \cf\SClinicBundle\Entity\Locals $local
     * @return Generator
     */
    public function setLocal(\cf\SClinicBundle\Entity\Locals $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \cf\SClinicBundle\Entity\Locals 
     */
    public function getLocal()
    {
        return $this->local;
    }
}
