<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PrepareDoses
 *
 * @ORM\Table(name="prepare_doses", indexes={@ORM\Index(name="FKprepare_do682278", columns={"medical_consultationid"}), @ORM\Index(name="FKprepare_do354899", columns={"radiopharmaceutical_id"}), @ORM\Index(name="FKprepare_do812786", columns={"generator_id"}), @ORM\Index(name="FKprepare_do649640", columns={"hood_id"})})
 * @ORM\Entity
 */
class PrepareDoses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="load_doses_datetime", type="datetime", nullable=false)
     */
    private $loadDosesDatetime;

    /**
     * @var float
     *
     * @ORM\Column(name="load_doses", type="float", precision=10, scale=0, nullable=false)
     */
    private $loadDoses;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Hood
     *
     * @ORM\ManyToOne(targetEntity="Hood")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hood_id", referencedColumnName="id")
     * })
     */
    private $hood;

    /**
     * @var \Radiopharmaceutical
     *
     * @ORM\ManyToOne(targetEntity="Radiopharmaceutical")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="radiopharmaceutical_id", referencedColumnName="id")
     * })
     */
    private $radiopharmaceutical;

    /**
     * @var \MedicalConsultation
     *
     * @ORM\ManyToOne(targetEntity="MedicalConsultation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medical_consultationid", referencedColumnName="id")
     * })
     */
    private $medicalConsultationid;

    /**
     * @var \Generator
     *
     * @ORM\ManyToOne(targetEntity="Generator")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="generator_id", referencedColumnName="id")
     * })
     */
    private $generator;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set loadDosesDatetime
     *
     * @param \DateTime $loadDosesDatetime
     * @return PrepareDoses
     */
    public function setLoadDosesDatetime($loadDosesDatetime)
    {
        $this->loadDosesDatetime = $loadDosesDatetime;

        return $this;
    }

    /**
     * Get loadDosesDatetime
     *
     * @return \DateTime 
     */
    public function getLoadDosesDatetime()
    {
        return $this->loadDosesDatetime;
    }

    /**
     * Set loadDoses
     *
     * @param float $loadDoses
     * @return PrepareDoses
     */
    public function setLoadDoses($loadDoses)
    {
        $this->loadDoses = $loadDoses;

        return $this;
    }

    /**
     * Get loadDoses
     *
     * @return float 
     */
    public function getLoadDoses()
    {
        return $this->loadDoses;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return PrepareDoses
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PrepareDoses
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return PrepareDoses
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PrepareDoses
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return PrepareDoses
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return PrepareDoses
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return PrepareDoses
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return PrepareDoses
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set hood
     *
     * @param \cf\SClinicBundle\Entity\Hood $hood
     * @return PrepareDoses
     */
    public function setHood(\cf\SClinicBundle\Entity\Hood $hood = null)
    {
        $this->hood = $hood;

        return $this;
    }

    /**
     * Get hood
     *
     * @return \cf\SClinicBundle\Entity\Hood 
     */
    public function getHood()
    {
        return $this->hood;
    }

    /**
     * Set radiopharmaceutical
     *
     * @param \cf\SClinicBundle\Entity\Radiopharmaceutical $radiopharmaceutical
     * @return PrepareDoses
     */
    public function setRadiopharmaceutical(\cf\SClinicBundle\Entity\Radiopharmaceutical $radiopharmaceutical = null)
    {
        $this->radiopharmaceutical = $radiopharmaceutical;

        return $this;
    }

    /**
     * Get radiopharmaceutical
     *
     * @return \cf\SClinicBundle\Entity\Radiopharmaceutical 
     */
    public function getRadiopharmaceutical()
    {
        return $this->radiopharmaceutical;
    }

    /**
     * Set medicalConsultationid
     *
     * @param \cf\SClinicBundle\Entity\MedicalConsultation $medicalConsultationid
     * @return PrepareDoses
     */
    public function setMedicalConsultationid(\cf\SClinicBundle\Entity\MedicalConsultation $medicalConsultationid = null)
    {
        $this->medicalConsultationid = $medicalConsultationid;

        return $this;
    }

    /**
     * Get medicalConsultationid
     *
     * @return \cf\SClinicBundle\Entity\MedicalConsultation 
     */
    public function getMedicalConsultationid()
    {
        return $this->medicalConsultationid;
    }

    /**
     * Set generator
     *
     * @param \cf\SClinicBundle\Entity\Generator $generator
     * @return PrepareDoses
     */
    public function setGenerator(\cf\SClinicBundle\Entity\Generator $generator = null)
    {
        $this->generator = $generator;

        return $this;
    }

    /**
     * Get generator
     *
     * @return \cf\SClinicBundle\Entity\Generator 
     */
    public function getGenerator()
    {
        return $this->generator;
    }
}
