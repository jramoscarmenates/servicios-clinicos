<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hood
 *
 * @ORM\Table(name="hood", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"}), @ORM\UniqueConstraint(name="stock_number", columns={"stock_number"})}, indexes={@ORM\Index(name="FKhood545972", columns={"local_id"})})
 * @ORM\Entity
 */
class Hood
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="stock_number", type="string", length=100, nullable=true)
     */
    private $stockNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="mark", type="string", length=100, nullable=true)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=100, nullable=true)
     */
    private $model;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="production_datetime", type="datetime", nullable=true)
     */
    private $productionDatetime;

    /**
     * @var float
     *
     * @ORM\Column(name="flow", type="float", precision=10, scale=0, nullable=true)
     */
    private $flow;

    /**
     * @var float
     *
     * @ORM\Column(name="pressure", type="float", precision=10, scale=0, nullable=true)
     */
    private $pressure;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_calibration_datetime", type="datetime", nullable=true)
     */
    private $lastCalibrationDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="last_calibration_certified", type="string", length=100, nullable=true)
     */
    private $lastCalibrationCertified;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Locals
     *
     * @ORM\ManyToOne(targetEntity="Locals")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="local_id", referencedColumnName="id")
     * })
     */
    private $local;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Hood
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set stockNumber
     *
     * @param string $stockNumber
     * @return Hood
     */
    public function setStockNumber($stockNumber)
    {
        $this->stockNumber = $stockNumber;

        return $this;
    }

    /**
     * Get stockNumber
     *
     * @return string 
     */
    public function getStockNumber()
    {
        return $this->stockNumber;
    }

    /**
     * Set mark
     *
     * @param string $mark
     * @return Hood
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Hood
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set productionDatetime
     *
     * @param \DateTime $productionDatetime
     * @return Hood
     */
    public function setProductionDatetime($productionDatetime)
    {
        $this->productionDatetime = $productionDatetime;

        return $this;
    }

    /**
     * Get productionDatetime
     *
     * @return \DateTime 
     */
    public function getProductionDatetime()
    {
        return $this->productionDatetime;
    }

    /**
     * Set flow
     *
     * @param float $flow
     * @return Hood
     */
    public function setFlow($flow)
    {
        $this->flow = $flow;

        return $this;
    }

    /**
     * Get flow
     *
     * @return float 
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * Set pressure
     *
     * @param float $pressure
     * @return Hood
     */
    public function setPressure($pressure)
    {
        $this->pressure = $pressure;

        return $this;
    }

    /**
     * Get pressure
     *
     * @return float 
     */
    public function getPressure()
    {
        return $this->pressure;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return Hood
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Hood
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Hood
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lastCalibrationDatetime
     *
     * @param \DateTime $lastCalibrationDatetime
     * @return Hood
     */
    public function setLastCalibrationDatetime($lastCalibrationDatetime)
    {
        $this->lastCalibrationDatetime = $lastCalibrationDatetime;

        return $this;
    }

    /**
     * Get lastCalibrationDatetime
     *
     * @return \DateTime 
     */
    public function getLastCalibrationDatetime()
    {
        return $this->lastCalibrationDatetime;
    }

    /**
     * Set lastCalibrationCertified
     *
     * @param string $lastCalibrationCertified
     * @return Hood
     */
    public function setLastCalibrationCertified($lastCalibrationCertified)
    {
        $this->lastCalibrationCertified = $lastCalibrationCertified;

        return $this;
    }

    /**
     * Get lastCalibrationCertified
     *
     * @return string 
     */
    public function getLastCalibrationCertified()
    {
        return $this->lastCalibrationCertified;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Hood
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Hood
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Hood
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Hood
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Hood
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set local
     *
     * @param \cf\SClinicBundle\Entity\Locals $local
     * @return Hood
     */
    public function setLocal(\cf\SClinicBundle\Entity\Locals $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \cf\SClinicBundle\Entity\Locals 
     */
    public function getLocal()
    {
        return $this->local;
    }
}
