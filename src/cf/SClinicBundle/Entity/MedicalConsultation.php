<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MedicalConsultation
 *
 * @ORM\Table(name="medical_consultation", indexes={@ORM\Index(name="FKmedical_co219233", columns={"paciente_id"}), @ORM\Index(name="FKmedical_co109913", columns={"medical_study_id"}), @ORM\Index(name="FKmedical_co793382", columns={"processingid"})})
 * @ORM\Entity
 */
class MedicalConsultation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_turn", type="date", nullable=false)
     */
    private $dateTurn;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", nullable=false)
     */
    private $age;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hour_turn", type="time", nullable=true)
     */
    private $hourTurn;

    /**
     * @var string
     *
     * @ORM\Column(name="doctor_makes_referral_name", type="string", length=100, nullable=true)
     */
    private $doctorMakesReferralName;

    /**
     * @var string
     *
     * @ORM\Column(name="doctor_makes_referral_id", type="string", length=50, nullable=true)
     */
    private $doctorMakesReferralId;

    /**
     * @var string
     *
     * @ORM\Column(name="hospital_make_referral_name", type="string", length=255, nullable=true)
     */
    private $hospitalMakeReferralName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_remission", type="datetime", nullable=true)
     */
    private $dateRemission;

    /**
     * @var integer
     *
     * @ORM\Column(name="off_calendar", type="smallint", nullable=true)
     */
    private $offCalendar;

    /**
     * @var float
     *
     * @ORM\Column(name="size", type="float", precision=10, scale=0, nullable=true)
     */
    private $size;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", precision=10, scale=0, nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="indication", type="text", nullable=true)
     */
    private $indication;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reception_datetime", type="date", nullable=true)
     */
    private $receptionDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="reception_user_id", type="integer", nullable=true)
     */
    private $receptionUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="pre_document", type="integer", nullable=true)
     */
    private $preDocument;

    /**
     * @var integer
     *
     * @ORM\Column(name="post_document", type="integer", nullable=true)
     */
    private $postDocument;

    /**
     * @var float
     *
     * @ORM\Column(name="price_cuc", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceCuc;

    /**
     * @var float
     *
     * @ORM\Column(name="price_cup", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceCup;

    /**
     * @var string
     *
     * @ORM\Column(name="number_of_acquire_by_doses", type="text", nullable=false)
     */
    private $numberOfAcquireByDoses;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Processing
     *
     * @ORM\ManyToOne(targetEntity="Processing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processingid", referencedColumnName="id")
     * })
     */
    private $processingid;

    /**
     * @var \MedicalStudy
     *
     * @ORM\ManyToOne(targetEntity="MedicalStudy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="medical_study_id", referencedColumnName="id")
     * })
     */
    private $medicalStudy;

    /**
     * @var \Patient
     *
     * @ORM\ManyToOne(targetEntity="Patient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     * })
     */
    private $paciente;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTurn
     *
     * @param \DateTime $dateTurn
     * @return MedicalConsultation
     */
    public function setDateTurn($dateTurn)
    {
        $this->dateTurn = $dateTurn;

        return $this;
    }

    /**
     * Get dateTurn
     *
     * @return \DateTime 
     */
    public function getDateTurn()
    {
        return $this->dateTurn;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return MedicalConsultation
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set hourTurn
     *
     * @param \DateTime $hourTurn
     * @return MedicalConsultation
     */
    public function setHourTurn($hourTurn)
    {
        $this->hourTurn = $hourTurn;

        return $this;
    }

    /**
     * Get hourTurn
     *
     * @return \DateTime 
     */
    public function getHourTurn()
    {
        return $this->hourTurn;
    }

    /**
     * Set doctorMakesReferralName
     *
     * @param string $doctorMakesReferralName
     * @return MedicalConsultation
     */
    public function setDoctorMakesReferralName($doctorMakesReferralName)
    {
        $this->doctorMakesReferralName = $doctorMakesReferralName;

        return $this;
    }

    /**
     * Get doctorMakesReferralName
     *
     * @return string 
     */
    public function getDoctorMakesReferralName()
    {
        return $this->doctorMakesReferralName;
    }

    /**
     * Set doctorMakesReferralId
     *
     * @param string $doctorMakesReferralId
     * @return MedicalConsultation
     */
    public function setDoctorMakesReferralId($doctorMakesReferralId)
    {
        $this->doctorMakesReferralId = $doctorMakesReferralId;

        return $this;
    }

    /**
     * Get doctorMakesReferralId
     *
     * @return string 
     */
    public function getDoctorMakesReferralId()
    {
        return $this->doctorMakesReferralId;
    }

    /**
     * Set hospitalMakeReferralName
     *
     * @param string $hospitalMakeReferralName
     * @return MedicalConsultation
     */
    public function setHospitalMakeReferralName($hospitalMakeReferralName)
    {
        $this->hospitalMakeReferralName = $hospitalMakeReferralName;

        return $this;
    }

    /**
     * Get hospitalMakeReferralName
     *
     * @return string 
     */
    public function getHospitalMakeReferralName()
    {
        return $this->hospitalMakeReferralName;
    }

    /**
     * Set dateRemission
     *
     * @param \DateTime $dateRemission
     * @return MedicalConsultation
     */
    public function setDateRemission($dateRemission)
    {
        $this->dateRemission = $dateRemission;

        return $this;
    }

    /**
     * Get dateRemission
     *
     * @return \DateTime 
     */
    public function getDateRemission()
    {
        return $this->dateRemission;
    }

    /**
     * Set offCalendar
     *
     * @param integer $offCalendar
     * @return MedicalConsultation
     */
    public function setOffCalendar($offCalendar)
    {
        $this->offCalendar = $offCalendar;

        return $this;
    }

    /**
     * Get offCalendar
     *
     * @return integer 
     */
    public function getOffCalendar()
    {
        return $this->offCalendar;
    }

    /**
     * Set size
     *
     * @param float $size
     * @return MedicalConsultation
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return float 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return MedicalConsultation
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set indication
     *
     * @param string $indication
     * @return MedicalConsultation
     */
    public function setIndication($indication)
    {
        $this->indication = $indication;

        return $this;
    }

    /**
     * Get indication
     *
     * @return string 
     */
    public function getIndication()
    {
        return $this->indication;
    }

    /**
     * Set receptionDatetime
     *
     * @param \DateTime $receptionDatetime
     * @return MedicalConsultation
     */
    public function setReceptionDatetime($receptionDatetime)
    {
        $this->receptionDatetime = $receptionDatetime;

        return $this;
    }

    /**
     * Get receptionDatetime
     *
     * @return \DateTime 
     */
    public function getReceptionDatetime()
    {
        return $this->receptionDatetime;
    }

    /**
     * Set receptionUserId
     *
     * @param integer $receptionUserId
     * @return MedicalConsultation
     */
    public function setReceptionUserId($receptionUserId)
    {
        $this->receptionUserId = $receptionUserId;

        return $this;
    }

    /**
     * Get receptionUserId
     *
     * @return integer 
     */
    public function getReceptionUserId()
    {
        return $this->receptionUserId;
    }

    /**
     * Set preDocument
     *
     * @param integer $preDocument
     * @return MedicalConsultation
     */
    public function setPreDocument($preDocument)
    {
        $this->preDocument = $preDocument;

        return $this;
    }

    /**
     * Get preDocument
     *
     * @return integer 
     */
    public function getPreDocument()
    {
        return $this->preDocument;
    }

    /**
     * Set postDocument
     *
     * @param integer $postDocument
     * @return MedicalConsultation
     */
    public function setPostDocument($postDocument)
    {
        $this->postDocument = $postDocument;

        return $this;
    }

    /**
     * Get postDocument
     *
     * @return integer 
     */
    public function getPostDocument()
    {
        return $this->postDocument;
    }

    /**
     * Set priceCuc
     *
     * @param float $priceCuc
     * @return MedicalConsultation
     */
    public function setPriceCuc($priceCuc)
    {
        $this->priceCuc = $priceCuc;

        return $this;
    }

    /**
     * Get priceCuc
     *
     * @return float 
     */
    public function getPriceCuc()
    {
        return $this->priceCuc;
    }

    /**
     * Set priceCup
     *
     * @param float $priceCup
     * @return MedicalConsultation
     */
    public function setPriceCup($priceCup)
    {
        $this->priceCup = $priceCup;

        return $this;
    }

    /**
     * Get priceCup
     *
     * @return float 
     */
    public function getPriceCup()
    {
        return $this->priceCup;
    }

    /**
     * Set numberOfAcquireByDoses
     *
     * @param string $numberOfAcquireByDoses
     * @return MedicalConsultation
     */
    public function setNumberOfAcquireByDoses($numberOfAcquireByDoses)
    {
        $this->numberOfAcquireByDoses = $numberOfAcquireByDoses;

        return $this;
    }

    /**
     * Get numberOfAcquireByDoses
     *
     * @return string 
     */
    public function getNumberOfAcquireByDoses()
    {
        return $this->numberOfAcquireByDoses;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return MedicalConsultation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MedicalConsultation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MedicalConsultation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return MedicalConsultation
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return MedicalConsultation
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return MedicalConsultation
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return MedicalConsultation
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set processingid
     *
     * @param \cf\SClinicBundle\Entity\Processing $processingid
     * @return MedicalConsultation
     */
    public function setProcessingid(\cf\SClinicBundle\Entity\Processing $processingid = null)
    {
        $this->processingid = $processingid;

        return $this;
    }

    /**
     * Get processingid
     *
     * @return \cf\SClinicBundle\Entity\Processing 
     */
    public function getProcessingid()
    {
        return $this->processingid;
    }

    /**
     * Set medicalStudy
     *
     * @param \cf\SClinicBundle\Entity\MedicalStudy $medicalStudy
     * @return MedicalConsultation
     */
    public function setMedicalStudy(\cf\SClinicBundle\Entity\MedicalStudy $medicalStudy = null)
    {
        $this->medicalStudy = $medicalStudy;

        return $this;
    }

    /**
     * Get medicalStudy
     *
     * @return \cf\SClinicBundle\Entity\MedicalStudy 
     */
    public function getMedicalStudy()
    {
        return $this->medicalStudy;
    }

    /**
     * Set paciente
     *
     * @param \cf\SClinicBundle\Entity\Patient $paciente
     * @return MedicalConsultation
     */
    public function setPaciente(\cf\SClinicBundle\Entity\Patient $paciente = null)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \cf\SClinicBundle\Entity\Patient 
     */
    public function getPaciente()
    {
        return $this->paciente;
    }
}
