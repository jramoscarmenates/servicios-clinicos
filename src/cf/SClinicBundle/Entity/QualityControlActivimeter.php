<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QualityControlActivimeter
 *
 * @ORM\Table(name="quality_control_activimeter", indexes={@ORM\Index(name="FKquality_co665487", columns={"activimeter_id"})})
 * @ORM\Entity
 */
class QualityControlActivimeter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \Activimeter
     *
     * @ORM\ManyToOne(targetEntity="Activimeter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="activimeter_id", referencedColumnName="id")
     * })
     */
    private $activimeter;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return QualityControlActivimeter
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return QualityControlActivimeter
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set activimeter
     *
     * @param \cf\SClinicBundle\Entity\Activimeter $activimeter
     * @return QualityControlActivimeter
     */
    public function setActivimeter(\cf\SClinicBundle\Entity\Activimeter $activimeter = null)
    {
        $this->activimeter = $activimeter;

        return $this;
    }

    /**
     * Get activimeter
     *
     * @return \cf\SClinicBundle\Entity\Activimeter 
     */
    public function getActivimeter()
    {
        return $this->activimeter;
    }
}
