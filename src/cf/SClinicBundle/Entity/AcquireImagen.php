<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AcquireImagen
 *
 * @ORM\Table(name="acquire_imagen", indexes={@ORM\Index(name="FKacquire_im918323", columns={"administer_doses_id"}), @ORM\Index(name="FKacquire_im226329", columns={"equipment_id"})})
 * @ORM\Entity
 */
class AcquireImagen
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acquire_datetime", type="datetime", nullable=false)
     */
    private $acquireDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \Equipment
     *
     * @ORM\ManyToOne(targetEntity="Equipment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipment_id", referencedColumnName="id")
     * })
     */
    private $equipment;

    /**
     * @var \AdministerDoses
     *
     * @ORM\ManyToOne(targetEntity="AdministerDoses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="administer_doses_id", referencedColumnName="id")
     * })
     */
    private $administerDoses;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return AcquireImagen
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set acquireDatetime
     *
     * @param \DateTime $acquireDatetime
     * @return AcquireImagen
     */
    public function setAcquireDatetime($acquireDatetime)
    {
        $this->acquireDatetime = $acquireDatetime;

        return $this;
    }

    /**
     * Get acquireDatetime
     *
     * @return \DateTime 
     */
    public function getAcquireDatetime()
    {
        return $this->acquireDatetime;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return AcquireImagen
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return AcquireImagen
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AcquireImagen
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return AcquireImagen
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return AcquireImagen
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return AcquireImagen
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return AcquireImagen
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set equipment
     *
     * @param \cf\SClinicBundle\Entity\Equipment $equipment
     * @return AcquireImagen
     */
    public function setEquipment(\cf\SClinicBundle\Entity\Equipment $equipment = null)
    {
        $this->equipment = $equipment;

        return $this;
    }

    /**
     * Get equipment
     *
     * @return \cf\SClinicBundle\Entity\Equipment 
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * Set administerDoses
     *
     * @param \cf\SClinicBundle\Entity\AdministerDoses $administerDoses
     * @return AcquireImagen
     */
    public function setAdministerDoses(\cf\SClinicBundle\Entity\AdministerDoses $administerDoses = null)
    {
        $this->administerDoses = $administerDoses;

        return $this;
    }

    /**
     * Get administerDoses
     *
     * @return \cf\SClinicBundle\Entity\AdministerDoses 
     */
    public function getAdministerDoses()
    {
        return $this->administerDoses;
    }
}
