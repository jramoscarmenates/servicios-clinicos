<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdministerDoses
 *
 * @ORM\Table(name="administer_doses", indexes={@ORM\Index(name="FKadminister277250", columns={"prepare_doses_id"})})
 * @ORM\Entity
 */
class AdministerDoses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="administer_datetime", type="datetime", nullable=false)
     */
    private $administerDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="zone", type="string", length=255, nullable=false)
     */
    private $zone;

    /**
     * @var string
     *
     * @ORM\Column(name="spacialist", type="string", length=255, nullable=false)
     */
    private $spacialist;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;

    /**
     * @var \PrepareDoses
     *
     * @ORM\ManyToOne(targetEntity="PrepareDoses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prepare_doses_id", referencedColumnName="id")
     * })
     */
    private $prepareDoses;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set administerDatetime
     *
     * @param \DateTime $administerDatetime
     * @return AdministerDoses
     */
    public function setAdministerDatetime($administerDatetime)
    {
        $this->administerDatetime = $administerDatetime;

        return $this;
    }

    /**
     * Get administerDatetime
     *
     * @return \DateTime 
     */
    public function getAdministerDatetime()
    {
        return $this->administerDatetime;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return AdministerDoses
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AdministerDoses
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set zone
     *
     * @param string $zone
     * @return AdministerDoses
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string 
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set spacialist
     *
     * @param string $spacialist
     * @return AdministerDoses
     */
    public function setSpacialist($spacialist)
    {
        $this->spacialist = $spacialist;

        return $this;
    }

    /**
     * Get spacialist
     *
     * @return string 
     */
    public function getSpacialist()
    {
        return $this->spacialist;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return AdministerDoses
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return AdministerDoses
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return AdministerDoses
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return AdministerDoses
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return AdministerDoses
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set prepareDoses
     *
     * @param \cf\SClinicBundle\Entity\PrepareDoses $prepareDoses
     * @return AdministerDoses
     */
    public function setPrepareDoses(\cf\SClinicBundle\Entity\PrepareDoses $prepareDoses = null)
    {
        $this->prepareDoses = $prepareDoses;

        return $this;
    }

    /**
     * Get prepareDoses
     *
     * @return \cf\SClinicBundle\Entity\PrepareDoses 
     */
    public function getPrepareDoses()
    {
        return $this->prepareDoses;
    }
}
