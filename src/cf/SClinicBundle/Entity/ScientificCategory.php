<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScientificCategory
 *
 * @ORM\Table(name="scientific category", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity
 */
class ScientificCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="acronym", type="string", length=50, nullable=true)
     */
    private $acronym;

    /**
     * @var string
     *
     * @ORM\Column(name="speciality", type="string", length=255, nullable=true)
     */
    private $speciality;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetie", type="datetime", nullable=false)
     */
    private $lastUpdateDatetie;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScientificCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set acronym
     *
     * @param string $acronym
     * @return ScientificCategory
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;

        return $this;
    }

    /**
     * Get acronym
     *
     * @return string 
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * Set speciality
     *
     * @param string $speciality
     * @return ScientificCategory
     */
    public function setSpeciality($speciality)
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return string 
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ScientificCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return ScientificCategory
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return ScientificCategory
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetie
     *
     * @param \DateTime $lastUpdateDatetie
     * @return ScientificCategory
     */
    public function setLastUpdateDatetie($lastUpdateDatetie)
    {
        $this->lastUpdateDatetie = $lastUpdateDatetie;

        return $this;
    }

    /**
     * Get lastUpdateDatetie
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetie()
    {
        return $this->lastUpdateDatetie;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return ScientificCategory
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }
}
