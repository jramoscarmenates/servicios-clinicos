<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QualityControlEquipment
 *
 * @ORM\Table(name="quality_control_equipment", indexes={@ORM\Index(name="FKquality_co517493", columns={"equipment_id"})})
 * @ORM\Entity
 */
class QualityControlEquipment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var \Equipment
     *
     * @ORM\ManyToOne(targetEntity="Equipment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="equipment_id", referencedColumnName="id")
     * })
     */
    private $equipment;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return QualityControlEquipment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return QualityControlEquipment
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set equipment
     *
     * @param \cf\SClinicBundle\Entity\Equipment $equipment
     * @return QualityControlEquipment
     */
    public function setEquipment(\cf\SClinicBundle\Entity\Equipment $equipment = null)
    {
        $this->equipment = $equipment;

        return $this;
    }

    /**
     * Get equipment
     *
     * @return \cf\SClinicBundle\Entity\Equipment 
     */
    public function getEquipment()
    {
        return $this->equipment;
    }
}
