<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MedicalStudy
 *
 * @ORM\Table(name="medical_study", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})}, indexes={@ORM\Index(name="FKmedical_st196853", columns={"types_equipment_id"}), @ORM\Index(name="FKmedical_st150998", columns={"report_templates_id"}), @ORM\Index(name="FKmedical_st645468", columns={"indications_id"})})
 * @ORM\Entity
 */
class MedicalStudy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="days_week_planned_and_amount", type="text", nullable=true)
     */
    private $daysWeekPlannedAndAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="number_of_acquire_by_doses", type="text", nullable=true)
     */
    private $numberOfAcquireByDoses;

    /**
     * @var float
     *
     * @ORM\Column(name="doses", type="float", precision=10, scale=0, nullable=false)
     */
    private $doses;

    /**
     * @var float
     *
     * @ORM\Column(name="price_cuc", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceCuc;

    /**
     * @var float
     *
     * @ORM\Column(name="price_cup", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceCup;

    /**
     * @var string
     *
     * @ORM\Column(name="administer_doses_zone", type="string", length=255, nullable=false)
     */
    private $administerDosesZone;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_datetime", type="integer", nullable=true)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=true)
     */
    private $createUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_datetime", type="integer", nullable=true)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=true)
     */
    private $lastUpdateUserId;

    /**
     * @var \IndicationsTemplates
     *
     * @ORM\ManyToOne(targetEntity="IndicationsTemplates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="indications_id", referencedColumnName="id")
     * })
     */
    private $indications;

    /**
     * @var \ReportTemplates
     *
     * @ORM\ManyToOne(targetEntity="ReportTemplates")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="report_templates_id", referencedColumnName="id")
     * })
     */
    private $reportTemplates;

    /**
     * @var \TypesEquipment
     *
     * @ORM\ManyToOne(targetEntity="TypesEquipment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="types_equipment_id", referencedColumnName="id")
     * })
     */
    private $typesEquipment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TypesRadiopharmaceutical", mappedBy="medicalStudy")
     */
    private $typesRadiopharmaceutical;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->typesRadiopharmaceutical = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MedicalStudy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set daysWeekPlannedAndAmount
     *
     * @param string $daysWeekPlannedAndAmount
     * @return MedicalStudy
     */
    public function setDaysWeekPlannedAndAmount($daysWeekPlannedAndAmount)
    {
        $this->daysWeekPlannedAndAmount = $daysWeekPlannedAndAmount;

        return $this;
    }

    /**
     * Get daysWeekPlannedAndAmount
     *
     * @return string 
     */
    public function getDaysWeekPlannedAndAmount()
    {
        return $this->daysWeekPlannedAndAmount;
    }

    /**
     * Set numberOfAcquireByDoses
     *
     * @param string $numberOfAcquireByDoses
     * @return MedicalStudy
     */
    public function setNumberOfAcquireByDoses($numberOfAcquireByDoses)
    {
        $this->numberOfAcquireByDoses = $numberOfAcquireByDoses;

        return $this;
    }

    /**
     * Get numberOfAcquireByDoses
     *
     * @return string 
     */
    public function getNumberOfAcquireByDoses()
    {
        return $this->numberOfAcquireByDoses;
    }

    /**
     * Set doses
     *
     * @param float $doses
     * @return MedicalStudy
     */
    public function setDoses($doses)
    {
        $this->doses = $doses;

        return $this;
    }

    /**
     * Get doses
     *
     * @return float 
     */
    public function getDoses()
    {
        return $this->doses;
    }

    /**
     * Set priceCuc
     *
     * @param float $priceCuc
     * @return MedicalStudy
     */
    public function setPriceCuc($priceCuc)
    {
        $this->priceCuc = $priceCuc;

        return $this;
    }

    /**
     * Get priceCuc
     *
     * @return float 
     */
    public function getPriceCuc()
    {
        return $this->priceCuc;
    }

    /**
     * Set priceCup
     *
     * @param float $priceCup
     * @return MedicalStudy
     */
    public function setPriceCup($priceCup)
    {
        $this->priceCup = $priceCup;

        return $this;
    }

    /**
     * Get priceCup
     *
     * @return float 
     */
    public function getPriceCup()
    {
        return $this->priceCup;
    }

    /**
     * Set administerDosesZone
     *
     * @param string $administerDosesZone
     * @return MedicalStudy
     */
    public function setAdministerDosesZone($administerDosesZone)
    {
        $this->administerDosesZone = $administerDosesZone;

        return $this;
    }

    /**
     * Get administerDosesZone
     *
     * @return string 
     */
    public function getAdministerDosesZone()
    {
        return $this->administerDosesZone;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MedicalStudy
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MedicalStudy
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDatetime
     *
     * @param integer $createDatetime
     * @return MedicalStudy
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return integer 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return MedicalStudy
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param integer $lastUpdateDatetime
     * @return MedicalStudy
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return integer 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return MedicalStudy
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }

    /**
     * Set indications
     *
     * @param \cf\SClinicBundle\Entity\IndicationsTemplates $indications
     * @return MedicalStudy
     */
    public function setIndications(\cf\SClinicBundle\Entity\IndicationsTemplates $indications = null)
    {
        $this->indications = $indications;

        return $this;
    }

    /**
     * Get indications
     *
     * @return \cf\SClinicBundle\Entity\IndicationsTemplates 
     */
    public function getIndications()
    {
        return $this->indications;
    }

    /**
     * Set reportTemplates
     *
     * @param \cf\SClinicBundle\Entity\ReportTemplates $reportTemplates
     * @return MedicalStudy
     */
    public function setReportTemplates(\cf\SClinicBundle\Entity\ReportTemplates $reportTemplates = null)
    {
        $this->reportTemplates = $reportTemplates;

        return $this;
    }

    /**
     * Get reportTemplates
     *
     * @return \cf\SClinicBundle\Entity\ReportTemplates 
     */
    public function getReportTemplates()
    {
        return $this->reportTemplates;
    }

    /**
     * Set typesEquipment
     *
     * @param \cf\SClinicBundle\Entity\TypesEquipment $typesEquipment
     * @return MedicalStudy
     */
    public function setTypesEquipment(\cf\SClinicBundle\Entity\TypesEquipment $typesEquipment = null)
    {
        $this->typesEquipment = $typesEquipment;

        return $this;
    }

    /**
     * Get typesEquipment
     *
     * @return \cf\SClinicBundle\Entity\TypesEquipment 
     */
    public function getTypesEquipment()
    {
        return $this->typesEquipment;
    }

    /**
     * Add typesRadiopharmaceutical
     *
     * @param \cf\SClinicBundle\Entity\TypesRadiopharmaceutical $typesRadiopharmaceutical
     * @return MedicalStudy
     */
    public function addTypesRadiopharmaceutical(\cf\SClinicBundle\Entity\TypesRadiopharmaceutical $typesRadiopharmaceutical)
    {
        $this->typesRadiopharmaceutical[] = $typesRadiopharmaceutical;

        return $this;
    }

    /**
     * Remove typesRadiopharmaceutical
     *
     * @param \cf\SClinicBundle\Entity\TypesRadiopharmaceutical $typesRadiopharmaceutical
     */
    public function removeTypesRadiopharmaceutical(\cf\SClinicBundle\Entity\TypesRadiopharmaceutical $typesRadiopharmaceutical)
    {
        $this->typesRadiopharmaceutical->removeElement($typesRadiopharmaceutical);
    }

    /**
     * Get typesRadiopharmaceutical
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTypesRadiopharmaceutical()
    {
        return $this->typesRadiopharmaceutical;
    }
}
