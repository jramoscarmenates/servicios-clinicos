<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoseReceived
 *
 * @ORM\Table(name="dose_received")
 * @ORM\Entity
 */
class DoseReceived
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="count", type="float", precision=10, scale=0, nullable=true)
     */
    private $count;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Worker", inversedBy="doseReceived")
     * @ORM\JoinTable(name="dose_received_worker",
     *   joinColumns={
     *     @ORM\JoinColumn(name="dose_received_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="worker_id", referencedColumnName="id")
     *   }
     * )
     */
    private $worker;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->worker = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set count
     *
     * @param float $count
     * @return DoseReceived
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return float 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Add worker
     *
     * @param \cf\SClinicBundle\Entity\Worker $worker
     * @return DoseReceived
     */
    public function addWorker(\cf\SClinicBundle\Entity\Worker $worker)
    {
        $this->worker[] = $worker;

        return $this;
    }

    /**
     * Remove worker
     *
     * @param \cf\SClinicBundle\Entity\Worker $worker
     */
    public function removeWorker(\cf\SClinicBundle\Entity\Worker $worker)
    {
        $this->worker->removeElement($worker);
    }

    /**
     * Get worker
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorker()
    {
        return $this->worker;
    }
}
