<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity
 */
class Report
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="report_datetime", type="datetime", nullable=false)
     */
    private $reportDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="specialist", type="string", length=255, nullable=false)
     */
    private $specialist;

    /**
     * @var string
     *
     * @ORM\Column(name="images_selected", type="string", length=255, nullable=true)
     */
    private $imagesSelected;

    /**
     * @var string
     *
     * @ORM\Column(name="pre_document", type="text", nullable=true)
     */
    private $preDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="text", nullable=true)
     */
    private $document;

    /**
     * @var string
     *
     * @ORM\Column(name="post_document", type="text", nullable=true)
     */
    private $postDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="medical_consultation_id", type="integer", nullable=false)
     */
    private $medicalConsultationId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_datetime", type="datetime", nullable=false)
     */
    private $createDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="create_user_id", type="integer", nullable=false)
     */
    private $createUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update_datetime", type="datetime", nullable=false)
     */
    private $lastUpdateDatetime;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_update_user_id", type="integer", nullable=false)
     */
    private $lastUpdateUserId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set reportDatetime
     *
     * @param \DateTime $reportDatetime
     * @return Report
     */
    public function setReportDatetime($reportDatetime)
    {
        $this->reportDatetime = $reportDatetime;

        return $this;
    }

    /**
     * Get reportDatetime
     *
     * @return \DateTime 
     */
    public function getReportDatetime()
    {
        return $this->reportDatetime;
    }

    /**
     * Set specialist
     *
     * @param string $specialist
     * @return Report
     */
    public function setSpecialist($specialist)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return string 
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set imagesSelected
     *
     * @param string $imagesSelected
     * @return Report
     */
    public function setImagesSelected($imagesSelected)
    {
        $this->imagesSelected = $imagesSelected;

        return $this;
    }

    /**
     * Get imagesSelected
     *
     * @return string 
     */
    public function getImagesSelected()
    {
        return $this->imagesSelected;
    }

    /**
     * Set preDocument
     *
     * @param string $preDocument
     * @return Report
     */
    public function setPreDocument($preDocument)
    {
        $this->preDocument = $preDocument;

        return $this;
    }

    /**
     * Get preDocument
     *
     * @return string 
     */
    public function getPreDocument()
    {
        return $this->preDocument;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return Report
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set postDocument
     *
     * @param string $postDocument
     * @return Report
     */
    public function setPostDocument($postDocument)
    {
        $this->postDocument = $postDocument;

        return $this;
    }

    /**
     * Get postDocument
     *
     * @return string 
     */
    public function getPostDocument()
    {
        return $this->postDocument;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Report
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Report
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set medicalConsultationId
     *
     * @param integer $medicalConsultationId
     * @return Report
     */
    public function setMedicalConsultationId($medicalConsultationId)
    {
        $this->medicalConsultationId = $medicalConsultationId;

        return $this;
    }

    /**
     * Get medicalConsultationId
     *
     * @return integer 
     */
    public function getMedicalConsultationId()
    {
        return $this->medicalConsultationId;
    }

    /**
     * Set createDatetime
     *
     * @param \DateTime $createDatetime
     * @return Report
     */
    public function setCreateDatetime($createDatetime)
    {
        $this->createDatetime = $createDatetime;

        return $this;
    }

    /**
     * Get createDatetime
     *
     * @return \DateTime 
     */
    public function getCreateDatetime()
    {
        return $this->createDatetime;
    }

    /**
     * Set createUserId
     *
     * @param integer $createUserId
     * @return Report
     */
    public function setCreateUserId($createUserId)
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    /**
     * Get createUserId
     *
     * @return integer 
     */
    public function getCreateUserId()
    {
        return $this->createUserId;
    }

    /**
     * Set lastUpdateDatetime
     *
     * @param \DateTime $lastUpdateDatetime
     * @return Report
     */
    public function setLastUpdateDatetime($lastUpdateDatetime)
    {
        $this->lastUpdateDatetime = $lastUpdateDatetime;

        return $this;
    }

    /**
     * Get lastUpdateDatetime
     *
     * @return \DateTime 
     */
    public function getLastUpdateDatetime()
    {
        return $this->lastUpdateDatetime;
    }

    /**
     * Set lastUpdateUserId
     *
     * @param integer $lastUpdateUserId
     * @return Report
     */
    public function setLastUpdateUserId($lastUpdateUserId)
    {
        $this->lastUpdateUserId = $lastUpdateUserId;

        return $this;
    }

    /**
     * Get lastUpdateUserId
     *
     * @return integer 
     */
    public function getLastUpdateUserId()
    {
        return $this->lastUpdateUserId;
    }
}
