<?php

namespace cf\SClinicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QualityControlHood
 *
 * @ORM\Table(name="quality_control_hood", indexes={@ORM\Index(name="FKquality_co49287", columns={"hood_id"})})
 * @ORM\Entity
 */
class QualityControlHood
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var \Hood
     *
     * @ORM\ManyToOne(targetEntity="Hood")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hood_id", referencedColumnName="id")
     * })
     */
    private $hood;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return QualityControlHood
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return QualityControlHood
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set hood
     *
     * @param \cf\SClinicBundle\Entity\Hood $hood
     * @return QualityControlHood
     */
    public function setHood(\cf\SClinicBundle\Entity\Hood $hood = null)
    {
        $this->hood = $hood;

        return $this;
    }

    /**
     * Get hood
     *
     * @return \cf\SClinicBundle\Entity\Hood 
     */
    public function getHood()
    {
        return $this->hood;
    }
}
