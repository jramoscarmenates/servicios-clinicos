/**
 * General functions.
 */

var is_mobile = false;
if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)) {
    is_mobile = true;
}

/**
 * Notifications messages.
 */

/**
 * Show messages from server.
 *
 * @param message
 */
function flashMessageLaunch(message) {
    if (message.type != 'none') {
        flashMessage(message.type, message.text, message.time, message.sticky, message.image, message.class_name);
    }
};

/**
 * Generate message depending of the type.
 *
 * @param type String Is the type of message (success, notification, warning, error).
 * @param text String Is the message to show.
 * @param time Time of show message.
 * @param sticky If want it to fade out on its own or just sit there.
 * @param image Path of image to show.
 * @param class_name Class css.
 */
function flashMessage(type, text, time, sticky, image, class_name) {
    var title = '';
    var path_default_image = 'bundles/cfsclinic/images/success.png';

    switch (type) {
        case 'success':
            title = '<span class="label label-success">' + type.toUpperCase() + '</span>';
            class_name = class_name + ' gritter-success';
            break;
        case 'notification':
            title = '<span class="label label-info">' + type.toUpperCase() + '</span>';
            class_name = class_name + ' gritter-info';
            break;
        case 'warning':
            title = '<span class="label label-warning">' + type.toUpperCase() + '</span>';
            class_name = class_name + ' gritter-warning';
            break;
        case 'error':
            title = '<span class="label label-danger">' + type.toUpperCase() + '</span>';
            path_default_image = 'bundles/cfsclinic/images/error.png';
            class_name = 'gritter-error';
            break;
    }

    $.gritter.add({
        title     : title,
        text      : text,
        image     : (image) ? image : path_default_image,
        sticky    : (sticky) ? sticky : false,
        time      : (time) ? time : '3000',
        class_name: (class_name) ? class_name : ''
    });
};