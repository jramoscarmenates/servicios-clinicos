'use strict';

var controllersModules = [
    // Vendors modules.
    'ngResource',
    'ngRoute',
    'localytics.directives',
    'angular.filter',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ui.bootstrap.transition',
    'ngCkeditor',
    'ngProgress',
    'siTable',
    'ngMessages',
    'restangular',

    // Filters module.
    'sclinic.filters',

    // Directives module.
    'sclinic.directives',

    // Controllers modules.
    'sclinic.default',
    'sclinic.users',
    'sclinic.indications_templates'
];

// Declare app level module which depends on filters, and services.
angular.module('sclinic', controllersModules)

    // Change {{}} by [[]].
    .config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }])

    // Restangular global configurations.
    .config(['RestangularProvider', function (RestangularProvider) {
        RestangularProvider

            .setBaseUrl('/api')// TODO: DIOSNE: La URL del API, sino coge la misma de Angularjs.

            .setRestangularFields({
                selfLink: 'url' // TODO: DIOSNE: Para q coja los enlaces q el API devuelve para q sepa donde
                                // TODO: DIOSNE: estan los elementos hijos y no tener q hacer nada a mano.
            })

            //.setDefaultHttpFields({cache: true})

            .setRequestInterceptor(function (elem, operation) {
                if (operation === "remove") {
                    return null;
                }
                return elem;
            })

            // TODO: DIOSNE: Esto fue copy & paste de la documentacion, hizo falta pa decirle
            // TODO: DIOSNE: q los datos estaban dentro de {data:[]}, en vez de en el root de la respuesta.
            .addResponseInterceptor(function (result, operation, what, url, response, deferred) {
                var extractedData;

                if (operation == 'getList') {
                    extractedData = result.data;
                }
                else {
                    extractedData = result;
                }

                return extractedData;
            });
    }])

    // Config of top progress bar.
    .config(['ngProgressProvider', function (ngProgressProvider) {
        // Default color is firebrick.
        ngProgressProvider.setColor('#892E65');
        // Default height is 2px.
        ngProgressProvider.setHeight('3px');
    }])

    // Routes of APP.
    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

        $routeProvider

            .otherwise({
                redirectTo: '/'
            });

        // TODO: VCA: Borrar esta línea si se llega a usar.
        //$locationProvider.hashPrefix('!');
    }])

    .run(['$rootScope', '$http', '$location', '$modal', '$log', '$timeout', 'Users', 'ngProgress', function ($rootScope, $http, $location, $modal, $log, $timeout, Users, ngProgress) {
        // Show or hide elements if error exist.
        $rootScope.error = false;

        // User logged.
        $rootScope.user = null;

        // Set project name.
        $rootScope.projectName = 'Clínica';

        /**
         * Options to show in each page.
         *
         * @type {{title: string, breadcrumbs: Array, pageHead: {}}}
         */
        $rootScope.optionsPage = {
            title      : $rootScope.projectName + ': Bienvenido', // Set title of pages (html-tag title).
            breadcrumbs: [], // Set breadcrumbs path. eg: [{icons:'ace-icon fa fa-home home-icon', url:'', name:'', active:false}]
            pageHead   : {} // Page head text. eg: {name: 'Dashboard', subtext: 'overview & stats'}
        }

        /**
         * Configuration by default of tables.
         *
         * @type {{}}
         */
        $rootScope.tableParams = {
            indices: 5, // How many indices show.
            limit  : 10, // Limit to show data.
            limits : [10, 25, 50, 100]
        }

        $rootScope.tableParams.errorTotal = {
            type: 'error',
            text: 'No se obtuvo el valor total de los datos obtenidos'
        }

        /**
         * Default options on toolbar of ckeditor.
         *
         * @type {*[]}
         */
        $rootScope.defaultToolbarFullCkeditor = [
            {
                name : 'basicstyles',
                items: ['Bold', 'Italic', 'Strike', 'Underline']
            },
            {name: 'paragraph', items: ['BulletedList', 'NumberedList', 'Blockquote']},
            {name: 'editing', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
            {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
            {name: 'tools', items: ['SpellChecker', 'Maximize']},
            '/',
            {
                name : 'styles',
                items: ['Format', 'FontSize', 'TextColor', 'BGColor', 'PasteText', 'PasteFromWord', 'RemoveFormat']
            },
            {name: 'insert', items: ['Image', 'Table', 'SpecialChar']},
            {name: 'forms', items: ['Outdent', 'Indent']},
            {name: 'clipboard', items: ['Undo', 'Redo']},
            {name: 'document', items: ['PageBreak', 'Source']}
        ];

        /**
         * Editor options by default.
         *
         * @type {{language: string, height: string, toolbar_full: *[], enterMode: number}}
         */
        $rootScope.editorOptions = {
            language    : 'es',
            height      : '100px',
            toolbar_full: (!!$rootScope.defaultToolbarFullCkeditor) ? $rootScope.defaultToolbarFullCkeditor : '',
            enterMode   : CKEDITOR.ENTER_BR
        };

        /**
         * Modal delete with message: Are you sure to delete this element?.
         * Reference: http://angular-ui.github.io/bootstrap/#/modal
         * Eg:
         * <button class="btn btn-default" ng-click="deleteModal(size, ctrl, tpl, element, index)">Delete</button>
         *
         * @param size      Size of modal ('sm' small or 'lg' large). eg: 'lg'.
         * @param ctrl      Any controller. eg: 'ApiIndicationsTemplatesController.delete'
         * @param tpl       Any Template by default is "delete_modal". eg: 'delete_modal'
         * @param element   Object to delete.
         * @param index     If element is into list get a index position in list.
         */
        $rootScope.deleteModal = function (size, ctrl, tpl, element, index) {
            var modalInstance = $modal.open({
                templateUrl: (tpl) ? tpl : 'delete_modal',
                controller : ctrl,
                size       : (size) ? size : 'lg',
                resolve    : {
                    data: function () {
                        return {
                            element: element,
                            index  : index
                        };
                    }
                }
            });

            modalInstance.result.then(function (result) {
                // TODO: VCA: Bloque que se ejecuta cuando se dé click en Ok.
            }, function () {
                // TODO: VCA: Bloque que se ejecuta cuando se dé click en Cancel.
            });
        };

        /**
         * Modal to show content from any controller with our template.
         * Reference: http://angular-ui.github.io/bootstrap/#/modal
         *
         * @param size          Size of modal ('sm' small or 'lg' large). eg: 'lg'.
         * @param css_class     Extra class CSS.
         * @param ctrl          Any controller. eg: 'ApiIndicationsTemplatesController.show'
         * @param tpl           Any Template. eg: 'indications_template_show'
         * @param element       Object to manipulate.
         * @param index     If element is into list get a index position in list.
         */
        $rootScope.openModal = function (size, css_class, ctrl, tpl, element, index) {
            var modalInstance = $modal.open({
                templateUrl: tpl,
                controller : ctrl,
                size       : (size) ? size : 'lg',
                windowClass: (css_class) ? css_class : '',
                resolve    : {
                    data: function () {
                        return {
                            element: element,
                            index  : index
                        };
                    }
                }
            });

            modalInstance.result.then(function (result) {
                // TODO: VCA: Bloque que se ejecuta cuando se dé click en Ok.
            }, function () {
                // TODO: VCA: Bloque que se ejecuta cuando se dé click en Cancel.
            });
        };

        $rootScope.$on('$routeChangeStart', function () {

            // Execute progress bar.
            ngProgress.start();
        })

        $rootScope.$on('$routeChangeSuccess', function () {

            // Finish progress bar.
            ngProgress.stop();
            ngProgress.complete();

            // Get current user.
            Users.customGET("", {code: 'current_user'})
                .then(function (result) {
                    flashMessageLaunch(result.meta.msg);
                    if (result.data) {
                        $rootScope.user = result.data;
                    }
                },
                function (error) {
                    flashMessageLaunch({type: 'error', text: error.statusText});
                });

            // Refresh menu page with current path.
            $rootScope.pathLocation = $location.path();
        });

        // Show page error.
        $rootScope.$on('db:error', function (event, meta) {
            if (!!meta.extra && !!meta.extra.error) {
                $location.path('/error');
                $rootScope.meta = meta;

                $rootScope.$broadcast('modal:close');
            }
        });
    }])
