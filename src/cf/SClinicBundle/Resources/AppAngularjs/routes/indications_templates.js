/**
 * Module Routes to work with sclinic.indications_templates module in frontend.
 */
'use strict';

angular.module("indications_templates.routes", [])
    .config(['$routeProvider', function ($routeProvider) {

        /**
         * Routes - ApiIndicationsTemplatesController.
         */

        $routeProvider

            .when('/indications_template', {
                templateUrl: '/bundles/cfsclinic/js/app/partials/IndicationsTemplates/index.html',
                controller : 'ApiIndicationsTemplatesController.index'
            })
    }])