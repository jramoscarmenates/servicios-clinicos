/**
 * Module Routes to work with sclinic.default module in frontend.
 */
'use strict';

angular.module("default.routes", [])
    .config(['$routeProvider', function ($routeProvider) {

        /**
         * Routes - DefaultController.
         */

        $routeProvider

            .when('/', {
                templateUrl: '/bundles/cfsclinic/js/app/partials/Default/index.html',
                controller : 'DefaultController.index'
            })

            .when('/error', {
                templateUrl: '/bundles/cfsclinic/js/app/partials/Default/error.html',
                controller : 'DefaultController.error'
            })
    }])