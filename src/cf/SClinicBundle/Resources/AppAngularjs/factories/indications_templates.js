/**
 * Module Models to work with sclinic.indications_templates module in frontend.
 */
'use strict';

angular.module("indications_templates.models", [])

    .factory('IndicationsTemplates', ['Restangular', function (Restangular) {
        return Restangular.all('indications-templates');
    }]);