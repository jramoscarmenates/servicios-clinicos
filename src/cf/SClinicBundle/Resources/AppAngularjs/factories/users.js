/**
 * Module Models to work with sclinic.user module in frontend.
 */
'use strict';

angular.module("users.models", [])

    .factory('Users', ['Restangular', function (Restangular) {
        return Restangular.all('users');
    }]);