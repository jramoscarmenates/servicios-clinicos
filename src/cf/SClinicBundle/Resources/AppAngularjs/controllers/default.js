/**
 * Module to work with DefaultController class in backend.
 */
'use strict';

angular.module("sclinic.default", ['default.routes'])

/**
 * Controllers by Routes.
 */

    .controller('DefaultController.index', ['$rootScope', '$location', function ($rootScope, $location) {
        $rootScope.optionsPage = {
            title      : $rootScope.projectName + ': Dashboard',
            breadcrumbs: [{
                icons : 'ace-icon fa fa-signal',
                url   : '',
                name  : 'Dashboard',
                active: true
            }],
            pageHead   : {
                name   : 'Dashboard',
                subtext: 'Resumen actividad'
            }
        }

        if ($rootScope.error) {
            // TODO: VCA: Ver cómo cargar una página en angularjs refrescando el navegador.
            $location.path('/');
        }
    }])

/**
 * Show error page.
 */
    .controller('DefaultController.error', ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {
        if (!!$rootScope.meta) {
            $scope.meta = $rootScope.meta;
            $rootScope.meta = null;
            $rootScope.error = true;

        } else {
            // TODO: VCA: Ver cómo cargar una página en angularjs refrescando el navegador.
            //var url = document.URL.split('#')[0] + 'login';
            //$('body').load('http://localhost:9991/login');
            $location.path('/');
        }
    }])