/**
 * Module to work with ApiIndicationsTemplatesController class in backend.
 */
'use strict';

angular.module("sclinic.indications_templates", ['indications_templates.routes', 'indications_templates.models'])

/**
 * Controllers by Routes.
 */

/**
 * Index controller.
 */
    // TODO: VCA: Quitar todos los parámetros que no se lleguen a usar, ellos están ahora para hacerles pruebas.
    .controller('ApiIndicationsTemplatesController.index', ['$scope', '$rootScope', 'IndicationsTemplates', function ($scope, $rootScope, IndicationsTemplates) {

        $rootScope.optionsPage = {
            title      : $rootScope.projectName + ': Modelos - Indicaciones',
            breadcrumbs: [{
                icons : 'ace-icon fa fa-book',
                url   : '',
                name  : 'Modelos',
                active: true
            }, {
                icons : '',
                url   : '',
                name  : 'Indicaciones',
                active: true
            }],
            pageHead   : {
                name   : 'Indicaciones',
                subtext: 'Modelos de Indicaciones Médicas'
            }
        }

        // Start Block to execute table.
        $scope.getData = function () {
            // Get init indications templates.
            IndicationsTemplates.customGET("", {
                code    : 'list',
                limit   : $scope.params.limit,
                offset  : $scope.params.offset,
                order_by: $scope.params.sortBy,
                search  : $scope.search
            }).then(function (result) { // Success.
                    $rootScope.$emit('db:error', result.meta);
                    flashMessageLaunch(result.meta.msg);
                    $rootScope.indications_templates = result.data;
                    $scope.status = result.meta.extra.status;
                    (!!result.meta.extra.parameter.count) ? $scope.params.total = result.meta.extra.parameter.count : flashMessageLaunch($rootScope.tableParams.errorTotal); // TODO: VCA: Recordar capturar valor aquí enviado por el server.
                    $scope.params.limit = $scope.params.limit; // TODO: VCA: Recordar capturar valor aquí enviado por el server.
                    $scope.init = false;
                },
                function (error) { // Error from server.
                    flashMessageLaunch({type: 'error', text: error.statusText});
                })
        }
        // End Block to execute table.
    }])

/**
 * Controllers without Routes.
 * These controllers have Templates instead of Routes.
 */

/**
 * Show indications template.
 */
    .controller('ApiIndicationsTemplatesController.show', ['$scope', '$rootScope', '$modalInstance', 'data', 'IndicationsTemplates', function ($scope, $rootScope, $modalInstance, data, IndicationsTemplates) {
        $scope.index = data.index;
        $scope.modalInstance = $modalInstance;

        IndicationsTemplates.get(data.element.id, {code: 'show'})
            .then(function (result) {
                $rootScope.$emit('db:error', result.meta);
                flashMessageLaunch(result.meta.msg)
                $scope.indication = result.data;
                $scope.title = result.data.name;

                $scope.status = result.meta.extra.status;
                $scope.status.forEach(function (current_status) {
                    if (current_status.selected) {
                        $scope.indication.status = current_status.value;
                    }
                });
            },
            function (error) { // Error from server.
                flashMessageLaunch({type: 'error', text: error.statusText});
            })

        $scope.getDataHistory = function () {
            if ($scope.indicationId) {
                IndicationsTemplates.get($scope.indicationId, {
                    code    : 'history',
                    limit   : $scope.params.limit,
                    offset  : $scope.params.offset,
                    order_by: $scope.params.sortBy,
                    search  : $scope.search
                }).then(function (result) {
                        $rootScope.$emit('db:error', result.meta);
                        flashMessageLaunch(result.meta.msg);
                        $scope.histories = result.data;
                        (!!result.meta.extra.parameter.count) ? $scope.params.total = result.meta.extra.parameter.count : flashMessageLaunch($rootScope.tableParams.errorTotal);
                        $scope.params.limit = (!!result.meta.extra.total) ? result.meta.extra.total : $scope.params.limit;
                        $scope.init = false;
                    },
                    function (error) { // Error from server.
                        flashMessageLaunch({type: 'error', text: error.statusText});
                    })
            }
        }

        // Get history of indication.
        $scope.history = function (id) {
            $scope.indicationId = id;
            $scope.getDataHistory();
        }

        // Load ckeditor notification.
        $scope.$on("ckeditor.ready", function (event) {
            $scope.isReady = true;
        });

        // Send message to close modals.
        $rootScope.$on('modal:close', function () {
            $modalInstance.dismiss('cancel');
        });
    }])

/**
 * Add indications template.
 */
    .controller('ApiIndicationsTemplatesController.add', ['$scope', '$rootScope', '$location', '$modalInstance', 'data', 'IndicationsTemplates', function ($scope, $rootScope, $location, $modalInstance, data, IndicationsTemplates) {
        $scope.modalInstance = $modalInstance;

        // Load ckeditor notification.
        $scope.$on("ckeditor.ready", function (event) {
            $scope.isReady = true;
        });

        /**
         * Get all status from ApiIndicationsTemplatesController in backend.
         */
        IndicationsTemplates.customGET()
            .then(function (result) { // Success.
                $rootScope.$emit('db:error', result.meta);
                flashMessageLaunch(result.meta.msg);
                $scope.status = result.meta.extra.status;

                $scope.status.forEach(function (current_status) {
                    if (current_status.selected) {
                        $scope.indicationAdd = {
                            status: current_status.value
                        }
                    }
                });
            },
            function (error) { // Error from server.
                flashMessageLaunch({type: 'error', text: error.statusText});
            });


        /**
         * Create indication in DB.
         *
         * @param indicationAdd
         */
        $scope.create = function (indicationAdd) {

            IndicationsTemplates.post(indicationAdd)
                .then(function (result) { // Success.
                    $rootScope.$emit('db:error', result.meta);
                    flashMessageLaunch(result.meta.msg);
                    if (!!$rootScope.indications_templates && !!result.data) {
                        if ($rootScope.indications_templates.length < 10) $rootScope.indications_templates.push(result.data);
                        $modalInstance.close();
                    }
                },
                function (error) { // Error from server.
                    flashMessageLaunch({type: 'error', text: error.statusText});
                });
        }
    }])

/**
 * Delete indication template.
 */
    .controller('ApiIndicationsTemplatesController.delete', ['$scope', '$rootScope', '$location', '$modalInstance', 'data', 'IndicationsTemplates', 'Restangular', function ($scope, $rootScope, $location, $modalInstance, data, IndicationsTemplates, Restangular) {
        $scope.title = data.element.name;

        // Delete Indication by ID.
        $scope.delete = function () {

            Restangular.one('indications-templates', data.element.id).remove()
                .then(function (result) {
                    $rootScope.$emit('db:error', result.meta);
                    flashMessageLaunch(result.meta.msg);
                    if (result && result.meta.msg.type == 'success') {
                        if (data.index == 0 || data.index > 0) {
                            $rootScope.indications_templates.splice(data.index, 1);

                            // Update table.
                            $rootScope.$emit('refresh:table');
                        } else {
                            $location.path('/indications_template');
                        }
                    }
                },
                function (error) { // Error from server.
                    flashMessageLaunch({type: 'error', text: error.statusText});
                });

            $modalInstance.close();
        };

        // Close modal that delete indication template.
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);