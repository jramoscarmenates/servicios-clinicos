/**
 * Module to work with ApiFosUserController class in backend.
 */
'use strict';

angular.module("sclinic.users", ['users.routes', 'users.models'])

/**
 * Controllers by Routes.
 */

