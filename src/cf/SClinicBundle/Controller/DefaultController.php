<?php

namespace cf\SClinicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('cfSClinicBundle:Default:index.html.twig', array());
    }
}
