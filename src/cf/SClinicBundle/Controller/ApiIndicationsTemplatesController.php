<?php

namespace cf\SClinicBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use cf\SClinicBundle\Entity\History;
use cf\SClinicBundle\Entity\IndicationsTemplates;

/**
 * IndicationsTemplates controller.
 *
 * @RouteResource("indications-templates")
 */
class ApiIndicationsTemplatesController extends FOSRestController{
    public $status;
    public $parameter;
    function __construct(){
        $this->status    = [['value' => '0', 'name' => 'desactivado', 'selected' => false, 'class' => 'label-danger arrowed arrowed-left'], ['value' => '1', 'name' => 'activado', 'selected' => true, 'class' => 'label-info arrowed arrowed-left',],];
        $this->parameter = ['length_row' => '10', 'count' => -1];
    }
    /**
     * Lists all Indications Templates entities.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function cgetAction(Request $request){
        try{
            $em     = $this->getDoctrine()->getManager();
            $params = $request->query->all();
            extract($params);
            $entities = [];
            $code     = isset($code) ? $code : null;
            switch($code){
                case 'list' :
                    $limit     = isset($limit) ? $limit : $this->parameter['length_row'];
                    $offset    = isset($offset) ? $offset : 0;
                    $order_by  = isset($order_by) ? $order_by : null;
                    $search    = isset($search) ? $search : null;
                    $arguments = compact('limit', 'offset', 'search', 'order_by');
                    $entities  = $em->getRepository('cfSClinicBundle:IndicationsTemplates')->findAllByNotDeleted($arguments, $this->parameter['count']);
                    break;
                default:
                    break;
            }

            return $this->get('cf.sclinicbundle.utils')->buildRestApi($entities, null, ['status' => $this->status, 'parameter' => $this->parameter]);
        }catch(\PDOException $e){
            //TODO: VCA: Recoger el MSG de error y lo muestre.
            //TODO: JCRC: Llevar a los logs en ficheros este error de problema de conexion con la DB.
            return $this->get('cf.sclinicbundle.utils')->buildRestApi([], ['type' => 'error', 'text' => 'Error al establecer conección con la Base de Datos.']);
        }
    }
    /**
     * Finds a IndicationsTemplates entity by id.
     *
     * @param Request $request
     * @param         $id
     *
     * @return array
     */
    public function getAction(Request $request, $id){
        try{
            $em       = $this->getDoctrine()->getManager();
            $msg      = [];
            $entities = [];
            // Find by id
            if(isset($id) && !empty($id) && is_numeric($id)){
                $params = $request->query->all();
                extract($params);
                $code = isset($code) ? $code : null;
                switch($code){
                    case 'show' :
                        $section_id = isset($id) ? $id : '-1';
                        try{
                            $entities = $em->getRepository('cfSClinicBundle:IndicationsTemplates')->findById($id);
                            if($entities){
                                $msg = [];
                            }
                        }catch(\Doctrine\ORM\NoResultException $e){
                            $msg = ['type' => 'error', 'msg' => 'No fue encontrado el ID.'];
                        }
                        break;
                    case 'history' :
                        // This section meabe go to the only get.
                        $limit      = isset($limit) ? $limit : 10;
                        $offset     = isset($offset) ? $offset : 0;
                        $section_id = isset($id) ? $id : '-1';
                        $entities   = $em->getRepository('cfSClinicBundle:History')->findAllByEntityAndSection('cfSClinicBundle:IndicationsTemplates', $section_id, $limit, $offset);
                        break;
                    default:
                        break;
                }
            }else{
                $msg = ['type' => 'error', 'msg' => 'Debe suministrar un ID correcto.'];
            }

            return $this->get('cf.sclinicbundle.utils')->buildRestApi($entities, $msg, ['status' => $this->status, 'parameter' => $this->parameter]);
        }catch(\PDOException $e){
            //TODO: JCRC: Llevar a los logs en ficheros este error de problema de conexion con la DB.
            return $this->get('cf.sclinicbundle.utils')->buildRestApi([], ['type' => 'error', 'text' => 'Error al establecer conección con la Base de Datos.']);
        }
    }
    /**
     * Create a new IndicationsTemplates entity.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function postAction(Request $request){
        // Get current user.
        $fosUser = $this->getUser(); //$this->get('security.context')->getToken()->getUser();
        if(!isset($fosUser) || !is_object($fosUser)){ // TODO: JCRC: Probar que con un usuario logueado capture el id y username.
            $fosUser = null;
        }
        $entity = new IndicationsTemplates($fosUser);
        $params = $request->request->all();
        extract($params);
        try{
            $em = $this->getDoctrine()->getManager('default');
            if(isset($name) && !empty($name)){
                if(!$em->getRepository('cfSClinicBundle:IndicationsTemplates')->findByName($name)){
                    $entity->setName($name);
                    if(isset($template_indication) && !empty($template_indication)){
                        $entity->setTemplateIndication($template_indication);
                    }
                    $entity->setStatus(0); // by json not sending values with 0.
                    if(isset($status) && !empty($status)){
                        $entity->setStatus($status);
                    }
                    if(isset($description) && !empty($description)){
                        $entity->setDescription($description);
                    }
                    $em->persist($entity);
                    $em->flush();
                    /* History */
                    $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "name", null, $name, $fosUser));
                    isset($template_indication) ? $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "template_indication", null, $template_indication, $fosUser)) : null;
                    isset($description) ? $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "description", null, $description, $fosUser)) : null;
                    $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "status", null, $entity->getStatus(), $fosUser));
                    $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "success - Indicación creada satisfactoriamente.", null, null, $fosUser));
                    $em->flush();

                    return $this->get('cf.sclinicbundle.utils')->buildRestApi($entity, ['type' => 'success', 'text' => 'Indicación creada satisfactoriamente.']);
                }else{
                    $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "error - Nombre duplicado.", null, $name, $fosUser));
                    $msg = ['type' => 'error', 'text' => 'Nombre duplicado.'];
                }

            }else{
                $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'create', $request->getClientIp(), "error - Debe especificar un nombre.", null, null, $fosUser));
                $msg = ['type' => 'error', 'text' => 'Debe especificar un nombre.'];
            }
            $em->flush();

            return $this->get('cf.sclinicbundle.utils')->buildRestApi(null, $msg);
        }catch(\PDOException $e){
            //TODO: JCRC: Llevar a los logs en ficheros este error de problema de conexion con la DB.
            return $this->get('cf.sclinicbundle.utils')->buildRestApi([], ['type' => 'error', 'text' => 'Error al establecer conección con la Base de Datos.']);
        }
    }

    /**
     * Creates a new IndicationsTemplates entity.
     *
     */
    /*public function createAction(Request $request)
    {
        $entity = new IndicationsTemplates();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

    }*/
    /**
     * Displays a form to edit an existing IndicationsTemplates entity.
     *
     */
    /*public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cfSClinicBundle:IndicationsTemplates')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IndicationsTemplates entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('cfSClinicBundle:IndicationsTemplates:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }*/
    /**
     * Edits an existing IndicationsTemplates entity.
     *
     */
    /*public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('cfSClinicBundle:IndicationsTemplates')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IndicationsTemplates entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('indicationstemplates_edit', array('id' => $id)));
        }

        return $this->render('cfSClinicBundle:IndicationsTemplates:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }*/
    /**
     * Deletes a IndicationsTemplates entity.
     *
     * @param Request $request
     * @param         $id
     *
     * @return mixed
     */
    public function deleteAction(Request $request, $id){
        // TODO: JCRC: Implementar la seguridad del sistema
        // Get current user.
        $fosUser = $this->getUser();
        if(!isset($fosUser) || !is_object($fosUser)){ // TODO: JCRC: Probar que con un usuario logueado capture el id y username.
            $fosUser = null;
        }
        try{
            $em = $this->getDoctrine()->getManager('default'); // ORM access
            if(isset($id) && is_numeric($id)){
                try{
                    //To Verify if exist ID to delete
                    $entity = $em->getRepository('cfSClinicBundle:IndicationsTemplates')->findById($id);
                    if(isset($entity) && !empty($entity)){
                        $date     = new \DateTime();
                        $datetime = $date->getTimestamp();
                        $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'delete', $request->getClientIp(), "name", $entity->getName(), null, $fosUser));
                        $entity->getTemplateIndication() ? $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'delete', $request->getClientIp(), "template_indication", $entity->getTemplateIndication(), null, $fosUser)) : null;
                        $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'delete', $request->getClientIp(), "status", $entity->getStatus(), null, $fosUser));
                        $entity->getDescription() ? $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'delete', $request->getClientIp(), "description", $entity->getDescription(), null, $fosUser)) : null;
                        /* Update parameters unique */
                        $entity->setName('d_'.$entity->getName().'_'.$datetime);
                        $entity->setStatus(-1); // Set status deleted.
                        $entity->setLastUpdateDatetime($date);
                        $entity->setLastUpdateUserId($fosUser->getId());
                        //                        $entity->setLastUpdateUserId(1); // TODO: JCRC: Eliminar cuando se terminen las pruebas.
                        $em->persist($entity);
                        $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'delete', $request->getClientIp(), "success - El elemento fue eliminado satisfactoriamented.", null, null, $fosUser));
                        $em->flush();
                        $msg = ['type' => 'success', 'text' => 'El elemento fue eliminado satisfactoriamented.'];

                        return $this->get('cf.sclinicbundle.utils')->buildRestApi(null, $msg, null, null);
                    }else{
                        $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $entity->getId(), 'delete', $request->getClientIp(), "error - El elemento a eliminar no fue encontrado.", null, null, $fosUser));
                        $msg = ['type' => 'error', 'text' => 'El elemento a eliminar no fue encontrado.'];
                    }
                }catch(\Doctrine\ORM\NoResultException $e){
                    $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $id, 'delete', $request->getClientIp(), "error - El elemento a eliminar no fue encontrado.", null, null, $fosUser));
                    $msg = ['type' => 'error', 'text' => 'El elemento a eliminar no fue encontrado.'];
                }
            }else{
                $em->persist(new History('cfSClinicBundle:IndicationsTemplates', $id, 'delete', $request->getClientIp(), "error - Debe introducir un id correcto.", null, null, $fosUser));
                $msg = ['type' => 'error', 'text' => 'Debe introducir un id correcto.'];
            }
            $em->flush();

            return $this->get('cf.sclinicbundle.utils')->buildRestApi(null, $msg, null, null);

        }catch(\PDOException $e){
            //TODO: VCA: Recoger el MSG de error y lo muestre.
            //TODO: JCRC: Llevar a los logs en ficheros este error de problema de conexion con la DB.
            return $this->get('cf.sclinicbundle.utils')->buildRestApi(null, ['type' => 'warning', 'text' => 'Error al establecer conección con la Base de Datos.']);
        }
    }
}
