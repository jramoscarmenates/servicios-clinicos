<?php

namespace cf\SClinicBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;

use cf\SClinicBundle\Util\UtilsExtensions;
use cf\SClinicBundle\Entity\FosUser;
use cf\SClinicBundle\Entity\FosGroup;

/**
 * User Api controller.
 *
 * @RouteResource("users")
 */
class ApiFosUserController extends FOSRestController
{
    /**
     * Get user.
     * To get a specific result, by example a current user following a next code type:
     * $request->get('code')
     * current_user -> Get current user.
     *
     * @param Request $request
     * @return array
     */
    public function getAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
	    $params = $request->query->all();
	    extract($params);

        // Get current user.
        $fosUser = $this->getUser();

	    $entities = array();
	    $msg = null;
	    $code = isset($code) ? $code : null;
        switch ($code) {
            // Code to get and return current user data.
            case 'current_user':
                if ($fosUser != 'anon.') {
                    $entities = $fosUser;
                } else {
	                $msg = array('type' => 'error', 'text' => 'Usuario no autenticado.');
                }
                break;
            default:
                $entities = $em->getRepository('cfSClinicBundle:FosUser')->findAll();
        }

        return $this->get('cf.sclinicbundle.utils')->buildRestApi($entities, $msg);
    }
}