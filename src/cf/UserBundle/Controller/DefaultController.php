<?php

namespace cf\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('cfUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
